import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})

export class DashboardService {

  @Output() change: EventEmitter<any> = new EventEmitter();

  public columsTable: string[];

  constructor(private http: HttpClient) {
    this.columsTable = [];
  }

  public getTechList(dashboardType: string = '') {
    return this.http.get(`${environment.baseUrl}${dashboardType}/tech/list`);
  }

  //MM
  public getBaselinesList(){
    return this.http.get(`${environment.baseUrl}/baseline/list`);
  }

  public getTemplateList(){
    return this.http.get(`${environment.baseUrl}/template/list`);
  }
  
  public getMOList(tech: string, mo?: string, limit?: number) {
    const params: any = { tech };
    if (mo !== undefined) {
      params.mo = mo;
    }
    if (limit !== undefined) {
      params.limit = limit;
    }

    return this.http.get(`${environment.baseUrl}/mo/list`, { params });
  }

  public getParamList(technology: string, mo: string) {
    const params = { technology, mo };
    return this.http.get(`${environment.baseUrl}/params/list`, { params });
  }

  public getDataParamList(technology: string, mo: string, id_baseline: string, fromTemplate: string) {
    const params = { technology, mo, id_baseline, fromTemplate };
    return this.http.get(`${environment.baseUrl}/data/params/list`, { params });
  }

  public getFilterParamList(tech: string, param: string, limit?: number) {
    const params: any = { tech, param };
    if (limit !== undefined) {
      params.limit = limit;
    }
    return this.http.get(`${environment.baseUrl}/params/filter/list`, { params });
  }

  public getColumnsData(param: any) {
    return this.http.get<any>(`${environment.baseUrl}/data/headers`, { params: param });
  }

  public saveNewBaseline(fromTemplate: string, 
                          baseline_name:string, 
                          vendor:string,
                          technology: string,
                          release: string,
                          description: string,
                          user_id: string,
                          user_name: string){
    const params: any = {fromTemplate,baseline_name,vendor,technology,release,description,user_id,user_name};
    return this.http.get(`${environment.baseUrl}/save/newBaseline`,{ params });
  }

  public saveChangesParamMo(param: string,oldValue:string, newValue:string,comment: string,
    userId: string, userName: string, mo: string,technology: string, id_baseline:string){
    const params: any ={param ,oldValue, newValue, comment ,userId, userName,mo,technology ,id_baseline };
    console.log(params);
    return this.http.get(`${environment.baseUrl}/save/changes/paramMo`, { params } );
  }

  public saveChangesBaselines(description: string, baseline_name:string, id_baseline:string){
    const params: any ={description ,baseline_name ,id_baseline };
    console.log(params);
    return this.http.get(`${environment.baseUrl}/save/changes/baselines`, { params } );
  }


  public saveMyChanges(credentials: any) {
    return this.http.post<any>(`${environment.baseUrl}/mychanges`, credentials);
  }

  public saveMyChangesBulk(credentials: any) {
    return this.http.post<any>(`${environment.baseUrl}/mychanges/bulk`, credentials);
  }

  public getFrequencyData(param: any) {
    return this.http.get<any>(`${environment.baseUrl}/data/frequency`, { params: param });
  }

  public getFrequencyDataForCsv(param: any): Observable<Blob> {
    return this.http.get(`${environment.baseUrl}/data/frequency`, { params: param, responseType: 'blob' });
  }

  public exportData(params: any) {
    return this.http.get(`${environment.baseUrl}/data/export`, { params: params });
  }

  public getDate() {
    return this.http.get<any>(`${environment.baseUrl}/date`);
  }

  public getTech(param: string) {
    return this.http.get<any>(`${environment.baseUrl}/tech/tree`, { params: { id: param } });

  }
}
