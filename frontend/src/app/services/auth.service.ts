import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { environment } from '../../environments/environment';
import { Authentication } from '../models/authentication';
import { Login } from '../models/login';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

    @Output() change: EventEmitter<User> = new EventEmitter();

  constructor(private http: HttpClient) { }

  public login(login: Login) {
    return this.http.post<Authentication>(`${environment.baseUrl}/user/login`, login);
  }

  public logout() {
      // remove user from local storage to log user out
      localStorage.removeItem(environment.localUser);
      localStorage.removeItem(environment.localToken);
  }

  public setToken(token: string) {
    if (token) {
      localStorage.setItem(environment.localToken, token);
    }
  }

  public getToken() {
    return localStorage.getItem(environment.localToken);
  }

  public setUser(user: User) {
    if (user) {
      localStorage.setItem(environment.localUser, JSON.stringify(user));
      this.change.emit(user);
    }
  }

  public getUser() {
    return JSON.parse(localStorage.getItem(environment.localUser));
  }

  public getUserInfo() {
    return this.http.get<User>(`${environment.baseUrl}/user`);
  }

  public resetPassword(emailParam: string) {
    const crendentials = {email: emailParam};
    return this.http.post(`${environment.baseUrl}/user/password/reset`, crendentials);
  }
}
