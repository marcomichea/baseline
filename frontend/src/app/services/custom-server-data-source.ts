import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table/lib/data-source/local/local.data-source';
import { of } from 'rxjs';
import { FixedData } from '../models/fixed-data';

@Injectable()
export class CustomServerDataSource extends LocalDataSource {

  @Output() triggerUpdate: EventEmitter<boolean> = new EventEmitter();
  @Output() sendTotalItems: EventEmitter<number> = new EventEmitter();

  private lastRequestCount: number = 0;
  private _url: string;
  private _dataSource: any;
  private _updateBand: boolean;
  private _perPageFilter: number = 30;
  updateElement: any;
  base_url;
  service: any;
  token: string;
  columsTable: string[] = [];
  preventLoad: boolean = false;
  lastData: any;
  environment: any;


  set perPageFilter(value) {
    if (value !== this._perPageFilter) {
      this._perPageFilter = value;
    }
  }

  get perPageFilter() {
    return this._perPageFilter;
  }

  get url() {
    return this._url;
  }

  set url(value) {
    this._url = value;
    this.pagingConf['page'] = 1;
    this.filterConf.filters = [];
  }

  get dataSource() {
    return this._dataSource;
  }

  set dataSource(value) {
    this._dataSource = value;
  }

  get updateBand() {
    return this._updateBand;
  }

  set updateBand(value) {
    this._updateBand = value;
  }

  constructor(protected http: HttpClient, service: any, url: string, token: string) {
    super();
    this.url = url;
    this.token = token;
    this.updateBand = false;
    this.service = service;
    this.http = http;
  }

  count(): number {
    return this.lastRequestCount;
  }

  getElements(): Promise<any> {
    if (this.updateBand) {
      this.updateBand = false;
      return new Promise((resolve, reject) => {
        if (this.dataSource)
          resolve(this.dataSource);
        else
          reject(Error('It broke'));
      });
    }

    this.base_url = this.url;
    if (this.sortConf) {
      this.sortConf.forEach((fieldConf) => {
        this.base_url += `&_sort=${fieldConf.field}&_order=${fieldConf.direction.toUpperCase()}&`;
      });
    }

    if (this.pagingConf) {
      this.base_url += `&rowsPerPage=${this.pagingConf['perPage']}`;
    }

    if (this.pagingConf && this.pagingConf['page']) {
      this.base_url += `&page=${this.pagingConf['page']}`;
    }

    if (this.filterConf.filters) {
      this.filterConf.filters.forEach((fieldConf) => {

        if (fieldConf['search']) {
          this.base_url += `&filter_${fieldConf['field']}=${fieldConf['search']}&`;
        }
      });
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`,
      }),
    };
    return this.http.get<any>(this.base_url, httpOptions).map(res => {
      if (res) {
        const serverData = res.result.data;
        if (serverData && serverData.length) {
          this.lastRequestCount = res.result.total;
          const data = [];
          let indexDb = res.result.from;
          for (let index = 0; index < serverData.length; index++) {
            const element = this.format(serverData[index], res.fixedData, indexDb);
            data.push(element);
            indexDb++;
          }
          this.sendTotalItems.emit(this.lastRequestCount);
          this.dataSource = data;
          this.data = this.dataSource;
          return this.dataSource;
        }
      }
      return this.sendEmptyData();
    }, (err: any) => {
      if (err.status === 401) {
        location.reload(true);
      }
      return this.sendEmptyData();
    }).catch(error => {
      if (error.status === 401) {
        location.reload(true);
      }
      return this.sendEmptyData();
    }).toPromise();
  }

  sendEmptyData() {
    this.sendTotalItems.emit(0);
    this.lastRequestCount = 0;
    this.data = [];
    this.dataSource = [];
    return [];
  }

  update(element: any, values: any): Promise<any> {
    this.updateBand = true;
    let found = this.dataSource.find(x => x.id === values.id);
    if (found) {
      found = Object.assign({}, values);
    }
    return super.update(element, values);
  }

  format(data: Object, fixedData: FixedData, indexDb: number): Object {
    const obj: Object = new Object();
    const keys = Object.keys(data);
    const values = Object.values(data);
    const separator = ':';
    let id = fixedData.date + separator + fixedData.tech + separator + fixedData.mo + separator + fixedData.param;
    // registrando datos dinamicos
    obj['date'] = fixedData.date;
    obj['mo'] = fixedData.mo;
    obj['param'] = fixedData.param;
    obj['tech'] = fixedData.tech;
    // datos propios del MO
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      if (key !== 'date') {
        obj[key] = values[i];
        if (key !== 'new_value' && key !== 'current_value')
          id += separator + values[i];
      }
    }

    obj['new_value'] = (obj['new_value']) ? obj['new_value'] : obj['current_value'];
    obj['indexDb'] = indexDb;
    obj['id'] = id;
    return obj;
  }
}
