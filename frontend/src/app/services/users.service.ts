import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { ServerDataSource } from 'ng2-smart-table';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { UserType } from '../enums/user-type.enum';


@Injectable({
  providedIn: 'root',
})
export class UsersService {

  private mapToServerUser = (user): User => {
    user.is_admin = user.type === UserType.Admin ? 1 : 0;
    return user;
  }

  private mapFromServerUser = (user): User => {
    user.type = user.is_admin ? UserType.Admin : UserType.User;
    return user;
  }

  constructor(
    private http: HttpClient,
  ) { }

  getUsersDataSource() {
    return new ServerDataSource(this.http, {
      endPoint: `${environment.baseUrl}/users`,
      pagerPageKey: 'page',
      pagerLimitKey: 'per_page',
      totalKey: 'total',
      dataKey: 'data',
    });
  }

  get(userId: number): Observable<User> {
    return this.http.get<any>(`${environment.baseUrl}/users/${userId}`)
      .pipe(map((user) => this.mapFromServerUser(user)));
  }

  getCurrentUser(): Observable<User> {
    return this.http.get<any>(`${environment.baseUrl}/user`)
      .pipe(map((user) => this.mapFromServerUser(user)));
  }

  create(user: any): Observable<User> {
    return this.http.post<User>(`${environment.baseUrl}/users`, this.mapToServerUser(user));
  }

  update(userId: number, user: any): Observable<User> {
    return this.http.put<User>(`${environment.baseUrl}/users/${userId}`, this.mapToServerUser(user));
  }

  delete(userId: number): Observable<any> {
    return this.http.delete(`${environment.baseUrl}/users/${userId}`);
  }

}
