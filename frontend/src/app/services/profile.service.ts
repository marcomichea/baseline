import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { PasswordReset } from '../models/password-reset';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {

  constructor(private http: HttpClient) { }

  public update(userInfo: User) {
    return this.http.put(`${environment.baseUrl}/user`, userInfo);
  }

  public changePassword(credentials: PasswordReset) {
    return this.http.put(`${environment.baseUrl}/user/password`, credentials);
  }
}
