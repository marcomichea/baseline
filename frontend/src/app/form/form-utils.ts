import { AbstractControl, FormArray, FormGroup } from '@angular/forms';

export function markAllControlsAsTouch(control: AbstractControl) {
  if (control instanceof FormGroup) {
    Object.keys(control.controls).forEach(field => {
      markAllControlsAsTouch(control.get(field));
    });
  } else if (control instanceof FormArray) {
    control.controls.forEach(field => {
      markAllControlsAsTouch(field);
    });
  }
  control.markAsTouched({ onlySelf: true });
}
