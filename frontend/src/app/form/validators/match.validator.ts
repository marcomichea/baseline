import { FormGroup } from '@angular/forms';

export function MatchValidator(fieldName: string, confirmationFieldName: string) {
  return (group: FormGroup): {[key: string]: any} => {
    const field = group.controls[fieldName];
    const confirmation = group.controls[confirmationFieldName];

    if (field.value !== confirmation.value) {
      return { ['match' + fieldName]: true };
    }
  };
}
