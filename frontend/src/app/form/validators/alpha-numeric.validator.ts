import { FormControl, ValidatorFn } from '@angular/forms';

const ALPHA_NUMERIC_REGEX = /^[a-zA-Z0-9-]*$/;

export function AlphaNumericValidator(): ValidatorFn {
  return (control: FormControl): {[key: string]: any} => {
    if (! ALPHA_NUMERIC_REGEX.test(control.value)) {
      return { alphanumeric: true };
    }
  };
}
