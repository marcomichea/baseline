import { FormControl, ValidatorFn } from '@angular/forms';

// tslint:disable-next-line:max-line-length
const PHONE_REGEX = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i;

export function PhoneValidator(): ValidatorFn {
  return (control: FormControl): {[key: string]: any} => {
    if (! PHONE_REGEX.test(control.value)) {
      return { phone: true };
    }
  };
}
