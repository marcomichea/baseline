import { FormControl, ValidatorFn } from '@angular/forms';

const ALPHA_SPACES_REGEX = /^[a-zA-Z\s]*$/;

export function AlphaSpacesValidator(): ValidatorFn {
  return (control: FormControl): {[key: string]: any} => {
    if (! ALPHA_SPACES_REGEX.test(control.value)) {
      return { alphaspaces: true };
    }
  };
}
