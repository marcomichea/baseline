export class FiltersParams {
    date: string;
    lastUpdateDate: string;
    tech: string;
    realTech: string;
    searchCriteria: string;
    mo: string;
    param: string;
    planned_area_id: string;
    column_name: string;
}

export enum SearchCriteria {
    MO = '0',
    Parameter = '1',
}

export enum ToastStatus {
    success = 'toast-success',
    info = 'toast-info',
    warning = 'toast-warning',
    primary = 'toast-primary',
    error = 'toast-error',
    default = 'toast-default',
}

export enum DashboardTypes {
    main = '',
    hw = '/hw',
}
