import { FixedData } from './fixed-data';
import { ResultServerData } from './result-server-data';

export class ServerData {
    fixedData: FixedData;
    result: ResultServerData;
}
