export class Baseline {
    id: number;
    is_template: number;
    baseline_name: string;
    vendor: string;
    technology: string;
    release: string;
    description: string;
    creation_date: string;
    last_update: string;
    user_id : number;
    fromTemplate: number;    
}