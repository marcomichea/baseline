import { FixedData } from './fixed-data';
export class TechLte extends FixedData {
    public id: number;
    public region: string;
    public sitio: string;
    public valor: number;
}
