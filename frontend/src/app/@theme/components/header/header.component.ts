import { Component, Input, OnInit } from '@angular/core';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { filter, map } from 'rxjs/operators';
import { LayoutService } from '../../../@core/data/layout.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';
import { User } from '../../../models/user';
import { AuthService } from '../../../services/auth.service';


@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  user: any;

  userMenu = [{
    title: 'Perfil',
    icon: 'nb-person',
    link: '/pages/perfil',
  }, {
    title: 'Cerrar Sesión',
    icon: 'nb-power',
  }];

  constructor(private sidebarService: NbSidebarService,
              private nbMenuService: NbMenuService,
              private menuService: NbMenuService,
              private authService: AuthService,
              private analyticsService: AnalyticsService,
              private layoutService: LayoutService) {
  }

  ngOnInit() {
    this.setUserProfile(this.authService.getUser());
    this.authService.change.subscribe(user => {
      this.setUserProfile(user);
    });
    this.nbMenuService.onItemClick()
    .pipe(
      filter(({ tag }) => tag === 'my-context-menu'),
      map(({ item: { title } }) => title),
    )
    .subscribe(title =>  {
      if (title.indexOf('Cerrar Sesi') >= 0) {
        this.authService.logout();
        location.reload(true);
      }
    });

  }

  setUserProfile(userInfo: User) {
    if (userInfo) {
      this.user = {
        name: `${userInfo.name} ${userInfo.last_name}`,
        picture: 'assets/images/nick.png',
      };
    }
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
}
