import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import * as html2canvas from 'html2canvas';
import { TreeviewItem } from 'ngx-treeview';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { User } from '../../models/user';
import { FiltersParams, ToastStatus } from '../../models/filters-params';
import { AuthService } from '../../services/auth.service';
import { CustomServerDataSource } from '../../services/custom-server-data-source';
import { DashboardService } from '../../services/dashboard.service';
import { ToastrService } from 'ngx-toastr';

enum SearchCriteria {
  MO = '0',
  Parameter = '1',
}

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  @ViewChild('contentchart') contChart: ElementRef;
  @ViewChild('generatebar') generateBar: ElementRef;
  @ViewChild('generateline') generateLine: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;
  @ViewChild('generatecsvbar') generateCsvBar: ElementRef;
  @ViewChild('generatecsvline') generateCsvLine: ElementRef;
  showLinkGenChart: boolean = false;

  observer: any;
  tempValNew: string;
  tempValOld: string;
  tempParam: string;
  tempConfirm: any;
  baseTemp: any;
  public user: User;

  techList: string[];
  moList: string[];
  paramList: string[] = [];
  searchCriteria: SearchCriteria = SearchCriteria.MO;
  ngModelDate: Date;
  filter: any;
  dataTableMo: string[];
  loadParamMo: string[];
  dataMapped: any[];
  // Valores seleccionados
  private _plannedArea: any = null;
  private _baselines: any = null;
  private _idBaseline: any;
  public tech: string;
  public fromTemplate :string;
  private _perPage: number = 30;
  private _checkBoxAll: any;
  private _allDataSet: boolean = false;
  private _realTech: string;
  mo: string;
  param: string;
  date: string;
  level3Click: boolean = false;

  messageSelectAllOption: string;
  messageSelectAll: string;

  private lastUpdateDate: string;

  // Listado de areas planificadas
  plannedAreaList: any[];
  loadingDataCsv: boolean;

  // Listado de Baselines
  baselinesList: any[];

  get baselines() {
    return this._baselines;
  }
  set baselines(val) {
    if (this._baselines !== val) {
      this._baselines = val;

      if (this._baselines !== null) {
        this.idBaseline = val.id_baseline;
        this.tech = val.technology;
        this.fromTemplate = val.fromTemplate.toString();
      }
      this.triggerFillTable();
    }
  }
  
  get idBaseline() {
    return this._idBaseline;
  }

  set idBaseline(val) {
    if (this._idBaseline !== val) {
      this._idBaseline = val;
      this.mo = '';
      this.param = '';
      //this.tech = this._realTech;
      this.fillTree(this._idBaseline.toString());
    }
  }

  get realTech() {
    return this._realTech;
  }

  set realTech(val) {
    if (this._realTech !== val) {
      this._realTech = val;
      this.mo = '';
      this.param = '';
      this.tech = this._realTech;
      this.fillTree(this._realTech);
    }
  }

  // borrar  ***I
  get plannedArea() {
    return this._plannedArea;
  }
  set plannedArea(val) {
    if (this._plannedArea !== val) {
      this._plannedArea = val;

      if (this._plannedArea !== null) {
        this.realTech = val.tech;
      }
      this.triggerFillTable();
    }
  }


  set perPage(value) {
    if (this._perPage !== value) {
      this.loadingData = true;
      this.source.setPaging(1, value);
      if (this.source) {
        if (this.checkBoxAll.checked) {
          this.checkBoxAll.click();
        }
      }
      this._perPage = value;
    }
  }

  get perPage() { return this._perPage; }

  set checkBoxAll(value) {
    if (value !== this._checkBoxAll)
      this._checkBoxAll = value;
  }

  get checkBoxAll() {
    return this._checkBoxAll;
  }

  set allDataSet(value) {
    this._allDataSet = value;
    if (!this._allDataSet) {
      // tslint:disable-next-line:max-line-length
      this.messageSelectAll = `Se han seleccionado ${this.selectedData.length} registros de esta página.`;
      this.messageSelectAllOption = `Clic para seleccionar los ${this.totalRecords} Registros`;
    } else {
      this.messageSelectAll = `Se han seleccionado ${this.totalRecords} registros.`;
      this.messageSelectAllOption = `Anular la selección`;
    }
  }

  get allDataSet() {
    return this._allDataSet;
  }

  totalRecords: number;

  // Datos para el treeview
  treeNodes: any;

  treeConfig = {
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: false,
    decoupleChildFromParent: false,
    maxHeight: 700,
  };

  // table vars
  baseColums = {
    mo: {
      title: 'MO',
      type: 'string',
      editable: false,
      filter: false,
      sort: false,
    },
  };
  columns: string[] = [];
  tableDataSource: any;

  settings = {
    //selectMode: 'multi',
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    actions: {
      columnTitle: 'Editar',
      add: false,
      delete: false,
      position: 'right',
    },
    columns: {
      param :{
        title: 'Parametro',        
        editable:false
      },
      template_value:{
        title: 'Valor Default',       
        editable:false
      },
      value :{
        title: 'Valor Baseline',       
        editable:true
      },
      comment :{
        title:'Comentario',
        editable: false
      }     
    },
    pager: {
      page: 1,
      perPage: this.perPage,
    },
    noDataMessage: `No se encontraron datos`,
    rowClassFunction: (row) => {
      if (row.data.current_value !== row.data.new_value) {
        return 'editedRow';
      } else {
        return '';
      }
    },
  };

  source: CustomServerDataSource;
  selectedData: any[];
  isAllData: boolean = false;
  showAllDataMessage: boolean = false;
  showPaginationSelect: boolean = false;

  // end table vars

  private tmpChangesId: number;
  loadingData: boolean = false;
  loadingChart: boolean = false;

  formatter = (result: any) => {
    if (this.searchCriteria === SearchCriteria.MO) {
       return result.technology + ' - ' + result.mo;
    } //else {
      //return result.tech + ' - ' + result.mo + ' - ' + result.column_name;
    //}
  }

  searchConditionally = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term => {
        if (term !== undefined && term.length > 2) {
          if (this.searchCriteria === SearchCriteria.MO) {
            return this.dashboardService.getMOList(this.tech, term, 15).pipe(
              catchError(() => {
                return of([]);
              }));
          } 
        } else {
          if (term.length <= 2) {
            this.fillTree(this.idBaseline.toString());
          }
          return of([]);
        }
      }),
    )

  public modalForm: FormGroup;
  paramForm: FormGroup;
  submitted: boolean;
  filtersParams: FiltersParams;

  constructor(
    private dialogService: NbDialogService,
    private auth: AuthService,
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private dashboardService: DashboardService,    
    protected dialogRef: NbDialogService,
  ) { }
  
  ngOnInit() {
    // Obtengo id si existe
    const id = this.route.snapshot.paramMap.get('id');
    
    this.user = this.auth.getUser();
    this.totalRecords = 0;
    //this.perPage = 30;
    this.selectedData = [];
    
    //Se carga el listado de los baselines guardados en la base de datos
    this.dashboardService.getBaselinesList().subscribe((res: any)=> {
      this.baselinesList = res;   
      
      if(id != null){      
        for (const base of this.baselinesList) {         
          if (base.baseline_name == id) {
            this.baseTemp = base;
          }
        }
        this.baselines = this.baseTemp;  
      }
    });
    
      this.modalForm = new FormGroup({
      comment: new FormControl(),
      language: new FormControl(),
    });

    this.paramForm = new FormGroup({
      new_value: new FormControl('', [
        Validators.required,
      ]),
    });
    this.submitted = true;
    this.addHorizontalTopScroll();
    this.bindHorizontalScroll();
    this.filtersParams = new FiltersParams();
  }

  private getTreeItem(items: any[], text: string, parentTech: string = '') {
    let found;
    for (let i = 0; i < items.length && !found; i++) {
      const item = items[i];
      if (item.text === text) {
        found = item;
      } else if (item.children && item.children.length > 0) {
        found = this.getTreeItem(item.children, text, parentTech);
      }
    }
    return found;
  }
 //TODO:
  fillTree(id : string) {    
    if (id && id.length > 0) {
      this.treeNodes = [];
      this.dashboardService.getTech(id).subscribe(
        (response: any) => {                
          if (response.length > 0) {
            for (let i = 0; i < response.length; i++) {
              const element =response[i].technology;                           
              this.treeNodes.push(new TreeviewItem({
                text: element,//[2] as string,                 
                value: { parent: null, level: 1 },
                children: [],
              }));
              this.loadMOList(element);
            }
          } else {
            this.showToast(ToastStatus.error, `No se pudo obtener el mo solicitado`);
          }
        }, err => {          
          this.showToast(ToastStatus.error, `Error obteniendo datos del arbol` + err);
        });
    } else {
      this.treeNodes = [];
    }
  }

  treeItemSelected(item) {
    this.level3Click = false;
    if (item.value.level < 3) {
      // Si no tiene hijos cargados
      if (!item.children || item.children.length === 0) {
        if (item.value.level === 1) {
          this.loadMOList(item.text);
        }
        if (item.value.level === 2) {
          this.tech = item.value.parent.text;         
          this.mo = item.text;
          this.param = item.text;
          
          //this.triggerFillTable();
          this.loadParamList(item.text, this.tech); 
          //this.loadDataParamList(this.mo);
          this.triggerFillTable();

        }
      } else {
        item.collapsed = !item.collapsed;
      }
    } else {
      this.level3Click = true;
      this.tech = item.value.parent.value.parent.text;
      this.mo = item.value.parent.text;
      this.param = item.text;
      this.triggerFillTable();
    }
  }

  isSelected(item) {
    const level = item.value.level;
    switch (level) {
      case 1:
        return item.text === this.tech && this.param;
      case 2:
        return item.text === this.mo && this.isSelected(item.value.parent);
      case 3:
        return item.text === this.param &&
          this.level3Click &&
          this.isSelected(item.value.parent);
    }
  }

  loadMOList(tech) {
    this.dashboardService.getMOList(tech).subscribe((res: any[]) => {
      const techItem = this.getTreeItem(this.treeNodes, tech);
      if (techItem) {
        techItem.children = res.map(function (moData) {
          return {
            text: moData.mo,
            value: { parent: techItem, level: 2 },
            children: [],
          };
        });
      }
    });
  }
 
  loadParamList(mo, parentTech: string = '') {
    this.dashboardService.getParamList(this.tech, mo).subscribe((res: string[]) => {
      const treeNodes: any = [];    
    });
  }
  //MM
  loadDataParamList(mo){
    
    this.dashboardService.getDataParamList(this.tech,mo,this.idBaseline,this.fromTemplate).subscribe((res: string[])=>{
      console.log(res);
    });
  }

  updateTree(item: any) {
    this.tech = item.technology;
    this.mo = item.mo;
    this.param = this.searchCriteria === SearchCriteria.MO ? undefined : item.column_name;
    // Mostramos la raiz (tech)
    this.treeNodes = [
      new TreeviewItem({
        text: this.tech,
        value: { parent: null, level: 1 },
        children: [],
      }),
    ];
    // Mostramos el mo
    this.treeNodes[0].children = [
      new TreeviewItem({
        text: this.mo,
        value: { parent: this.treeNodes[0], level: 2 },
        children: [],
      }),
    ];
    this.triggerFillTable();
    //this.loadParamList(this.mo, this.tech);
    if (this.searchCriteria === SearchCriteria.Parameter) {
      this.triggerFillTable();
    }
  }
  // region modal

  openConfirmDialog(dialog: TemplateRef<any>, event: any,  multi: boolean = false ) {
   
    this.tempValNew = event.newData.value;
    this.tempValOld = event.data.value;
    this.tempParam = event.data.param;   
    this.tempConfirm = event;

    this.modalForm.reset();

    this.dialogService.open(dialog, {
      context: {
        _event: event,
      },
    });
 }

  confirmSave(ref) {
    const val = this.modalForm.value;    
    if (val.comment) {
      const params = {
        oldValue: this.tempValOld,
        newValue: this.tempValNew,
        param: this.tempParam,
        comment: val.comment,
        userId : this.user.id,
        userName: this.user.name
      };
      //guardar aqui datos actualizados
      this.updateChangeTable(params, event);
      
      //this.saveMyChanges(params, ref);
    } else {
      this.showToast(ToastStatus.warning, `No hay datos que guardar`);
      this.submitted = false;
    }
    ref.close();
  }

  saveMyChanges(params: any, modalRef) {
    this.dashboardService.saveMyChanges(params).subscribe((response: any) => {
      if (response.success) {
        this.showToast(ToastStatus.success, response.success);
        this.submitted = true;
        this.modalForm.reset();
        modalRef.close();
        this.router.navigate([`/pages/miscambios/editar`, response.id]);
      } else {
        this.showToast(ToastStatus.error, `Algunos datos no pudieron ser guardados`);
      }
    }, err => {
      let msg = '';
      if (typeof err.error.error === 'string') {
        msg = err.error.error;
      } else if (err.error.error.name) {
        msg = err.error.error.name[0];
      } else {
        msg = `Ocurrió un problema en el servidor`;
      }
      this.showToast(ToastStatus.error, msg);
    });
  }

  // endregion


  // region datepicker
  handleDateChange(event) {
    this.date = event ? event.toISOString().split('T')[0] : this.lastUpdateDate;
    this.triggerFillTable();
  }
  // endregion

  prepareParamData(): any {
    const params = {
      tech: this.tech,
      mo: this.mo,
      param: this.param,
      date: this.date,
      planned_area_id: this.plannedArea ? this.plannedArea.id : '',
    };
    if (this.source.getFilter().filters) {
      const filters = this.source.getFilter().filters;
      filters.forEach((fieldConf) => {
        if (fieldConf['search']) {
          params[`filter_${fieldConf['field']}`] = `${fieldConf['search']}`;
        }
      });
    }

    return params;
  }

  prepareParamDataForExcel(): any {
    const params = {
      tech: this.tech,
      mo: this.mo,
      param: this.param,
      date: this.date,
      planned_area_id: this.plannedArea ? this.plannedArea.id : '',
      excel: true,
    };
    if (this.source.getFilter().filters) {
      const filters = this.source.getFilter().filters;
      filters.forEach((fieldConf) => {
        if (fieldConf['search']) {
          params[`filter_${fieldConf['field']}`] = `${fieldConf['search']}`;
        }
      });
    }

    return params;
  }

  // region table functions

  private triggerFillTable() {
    if (this.tech && this.mo && this.param) { //&& this.param
      this.fillTable();
    }
  }

  fillTable() {
    
    this.loadingData = true;
    this.allDataSet = false;
        
     this.dashboardService.getDataParamList(this.tech,this.mo,this.idBaseline, this.fromTemplate).subscribe((res: string[])=>{
      this.dataTableMo = res;
      const obj2 = this.dataTableMo[2];
      const obj1 = this.dataTableMo[1];
      const obj =this.dataTableMo[0];            
      
      this.dataMapped = Object.keys(obj).map(key => ({param: key, template_value:obj[key] ,value:obj1[key], comment:obj2[key]}));
      this.refreshDashboardData();
      this.loadingData = false;

    });    
  }

  refreshDashboardData() {
    this.allDataSet = false;
    if (this.checkBoxAll && this.checkBoxAll.checked) {
      this.checkBoxAll.click();
    }
    //this.fillCharts();   MM
  }

  createTableColumns() {
    /*if (this.columns.length > 0) {
      this.settings.columns = Object.assign({}, this.baseColums);
      for (let i = 0; i < this.columns.length; i++) {
        const element = this.columns[i];
        if (element !== 'tecnologia' && element !== 'fecha'
          && element !== 'parametro' && element !== 'new_value'
          && element !== 'valor') {
          this.addColumnTable(element, {
            title: element.replace('_', ' ').replace(/\b\w/g, l => l.toUpperCase()),
            type: 'string',
            editable: false,
            sort: false,
          });
        }
      }
    }*/
    this.addColumnTable('param', {
      title: 'Parámetro',
      type: 'string',
      editable: false,
      sort: false,
    });

    this.addColumnTable('new_value', {
      title: 'Valor',
      type: 'string',
      editable: true,
      sort: false,
    });
  }

  addColumnTable(name: string, properties: Object) {
    this.settings.columns[name] = properties;
    this.settings = Object.assign({}, this.settings);
  }

  confirmDelete(ev: any, dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, {
      context: {
        user: ev.data,
      },
    });
  }

  /*
  saveChanges(event: any, multi: boolean = false) {
          
    const data = {
      details: (multi) ? event : [event.newData],
    };   
    if (event.data.new_value === event.newData.new_value) {
      this.updateChangeTable(event);
    } else {
      this.sendSaveChanges(data);
    }
  }*/

  sendSaveChanges(data, page: number = 1) {
    this.loadingData = true;
    this.dashboardService.saveMyChanges(data).subscribe(
      (response: any) => {
        this.tmpChangesId = response.id;
        this.showToast(ToastStatus.success, response.success);
        this.source.setPage(page);
        this.selectedData = [];
      }, err => {
        let message = '';
        if (err.error.error['0.new_value']) {
          message = err.error.error['0.new_value'][0];
        } else {
          message = `Ocurrió un error durante la actualización del cambio.`;
        }
        this.showToast(ToastStatus.error, message);
      },
    );
  }

  saveMyChangesBulk(newValue: string) {
    const params = this.prepareParamData();
    params['new_value'] = newValue;
    this.dashboardService.saveMyChangesBulk(params)
      .subscribe(response => {
        this.showToast(ToastStatus.success, response.success);
        this.fillTable();
      }, err => {
        this.showToast(ToastStatus.error, `Ocurrio un error guardando los datos`);
      });
  }

  updateChangeTable(param, event) {   
    
    this.dashboardService.saveChangesParamMo(param.param,param.oldValue, param.newValue, param.comment , param.userId, param. userName, this.mo,this.tech, this.idBaseline)
    .subscribe(response =>{
    
      this.showToast(ToastStatus.success, `Registro guardado exitosamente`);
      this.fillTable();      
      this.tempConfirm.confirm.resolve();
            
    }, err => {
      this.showToast(ToastStatus.error, `Ocurrio un error guardando los datos`);
    });

    
  }

  onUserRowSelect(event) {
    this.selectedData = event.selected;
    const lastItem = (this.selectedData.length > 0)
      ? this.selectedData[this.selectedData.length - 1]
      : { indexDb: 0 };
    const perpage = typeof (this.perPage) === 'string' ? parseInt(this.perPage) : this.perPage;

    if (this.selectedData.length === this.totalRecords ||
      this.selectedData.length === perpage ||
      this.totalRecords === lastItem.indexDb) {
      this.checkBoxAll.checked = true;
      this.allDataSet = false;
      this.showAllDataMessage = (this.selectedData.length !== this.totalRecords);
    } else {
      this.checkBoxAll.checked = false;
      this.showAllDataMessage = false;
      this.allDataSet = false;
    }
  }

  openDialogMultiUpdate(dialogMultiUpdate: TemplateRef<any>) {
    this.paramForm.reset();
    this.dialogService.open(dialogMultiUpdate, { context: '' });
  }

  saveMultiUpdate(ref) {
    const val = this.paramForm.value;
    if (this.selectedData.length > 0 && this.paramForm.valid) {
      if (this.allDataSet) {
        this.saveMyChangesBulk(val.new_value);
      } else {
        for (let i = 0; i < this.selectedData.length; i++) {
          const element = this.selectedData[i];
          element.new_value = val.new_value;
        }
        const data = {
          details: this.selectedData,
        };

        if (this.source.getFilter()) {
          this.filter = this.source.getFilter().filters;
        }
        this.sendSaveChanges(data, this.source.getPaging().page);
      }
      ref.close();
    }
  }

  clickAllData($evt) {
    this.allDataSet = !this.allDataSet;
    return false;
  }
  // endregion

  showToast(status, message) {
    const icon = status;
    this.toastr.show('', message, {}, status);
  }

  clickEventHandler($evt) {
    const chart = this.contChart['flipped'] ? 'lineChart' : 'barChart';
    this.generateBar.nativeElement.hidden = true;
    this.generateLine.nativeElement.hidden = true;
    this.generateCsvBar.nativeElement.hidden = true;
    this.generateCsvLine.nativeElement.hidden = true;
    const data = document.getElementById(chart);
    this.downloadLink.nativeElement.target = '_blank';
    html2canvas(data, { logging: false }).then((canvas: any) => {
      this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
      // tslint:disable-next-line:max-line-length
      const name = `${this.date.replace('/', '-').toLowerCase()}-${this.tech.toLowerCase()}-${this.mo.toLowerCase()}-${this.param.toLowerCase()}-${chart}-diagram.png`;
      this.downloadLink.nativeElement.download = name;
      this.downloadLink.nativeElement.click();
      this.generateBar.nativeElement.hidden = false;
      this.generateLine.nativeElement.hidden = false;
      this.generateCsvBar.nativeElement.hidden = false;
      this.generateCsvLine.nativeElement.hidden = false;
    });
    return false;
  }

  clickEventHandlerCsv($evt) {
    const params = this.prepareParamDataForExcel();

    if (this.totalRecords <= 0) {
      this.settings.noDataMessage = `No se encontraron datos`;
    }
    this.dashboardService.getFrequencyDataForCsv(params).subscribe((response: Blob) => {
      const a = document.createElement('a');
      const blob = new Blob([response], { type: 'text/csv' });
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      // tslint:disable-next-line:max-line-length
      const name = `${this.date.replace('/', '-').toLowerCase()}-${this.tech.toLowerCase()}-${this.mo.toLowerCase()}-${this.param.toLowerCase()}-datos_grafico.csv`;
      a.download = name;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    }, err => {
      this.showToast(ToastStatus.error, `Ocurrió un error obteniendo datos del gráfico`);

      this.showLinkGenChart = false;
    });

    return false;
  }

  clickEventHandlerCsvTable($evt) {
    if (!this.loadingDataCsv) {
      this.filtersParams.planned_area_id = this.plannedArea ? this.plannedArea.id : '';
      this.filtersParams.tech = this.tech;
      this.filtersParams.mo = this.mo;
      this.filtersParams.param = this.param;
      this.filtersParams.date = this.date;
      this.resetFilters();
      this.bindFilterTable();
      this.loadingDataCsv = true;
      this.dashboardService.exportData(this.filtersParams).subscribe((response: any) => {
        if (response.status === 'success') {
          const status = response.status;
          const icon = response.status;
          this.toastr.show(response.message, response.title, {}, ToastStatus.success);
        }
        this.loadingDataCsv = false;
      }, err => {
        this.showToast(ToastStatus.error, `Ocurrió un error obteniendo datos en csv`);
        this.showLinkGenChart = false;
        this.loadingDataCsv = false;
      });
    }

    return false;
  }

  bindFilterTable() {
    if (this.source.getFilter().filters) {
      const filters = this.source.getFilter().filters;
      filters.forEach((fieldConf: any) => {
        if (fieldConf['search']) {
          this.filtersParams[`filter_${fieldConf['field']}`] = `${fieldConf['search']}`;
        }
      });
    }
  }

  addHorizontalTopScroll() {
    let topContainer: any = null;
    this.observer = new MutationObserver(function (mutations) {
      const tmp = document.querySelector('#dashboardTable table');
      if (tmp.clientWidth) {
        topContainer = (topContainer) ? topContainer : document.getElementById('topScroll');
        topContainer.style.width = tmp.clientWidth + 'px';
      }
    });

    this.observer.observe(document.getElementById('wrapper2'), { subtree: true, childList: true });
  }

  bindHorizontalScroll() {
    const wrapperTop = document.getElementById('wrapper1');
    const tableWrapper = document.getElementById('wrapper2');
    wrapperTop.onscroll = (evt) => {
      tableWrapper.scrollLeft = wrapperTop.scrollLeft;
    };

    tableWrapper.onscroll = (evt) => {
      wrapperTop.scrollLeft = tableWrapper.scrollLeft;
    };
  }

  resetFilters() {
    delete this.filtersParams['excel'];
    // tslint:disable-next-line:forin
    for (const key in this.filtersParams) {
      if (key.indexOf('filter') >= 0) {
        delete this.filtersParams[key];
      }
    }
  }
}
