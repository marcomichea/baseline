import { NgModule } from '@angular/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
// tslint:disable-next-line:max-line-length
import { NbAlertModule, NbDatepickerModule, NbDialogModule, NbPopoverModule, NbRadioModule, NbSpinnerModule } from '@nebular/theme';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TreeModule } from 'angular-tree-component';
import { ChartModule } from 'angular2-chartjs';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxPaginationModule, PaginatePipe } from 'ngx-pagination';
import { TreeviewModule } from 'ngx-treeview';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    ThemeModule,
    ChartModule,
    NgxEchartsModule,
    NgxChartsModule,
    LeafletModule,
    TreeModule,
    Ng2SmartTableModule,
    NbDatepickerModule,
    NbPopoverModule,
    NbDialogModule.forRoot(),
    NbRadioModule,
    TreeviewModule.forRoot(),
    ToastrModule.forRoot(),
    NbAlertModule,
    NbSpinnerModule,
    NgxPaginationModule,
  ],
  declarations: [
    DashboardComponent,
  ],
  providers: [
    PaginatePipe,
  ],
})
export class DashboardModule { }
