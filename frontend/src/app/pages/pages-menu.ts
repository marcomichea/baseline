import { NbMenuItem } from '@nebular/theme';

const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Nuevo Baseline',
    icon: 'nb-compose',
    link: '/pages/newBaseline',
  },  
  {
    title: 'Baselines',
    icon: 'nb-list',
    link: '/pages/baselines',
  }
];

const ADMIN_MENU_ITEM: NbMenuItem = {
  title: 'Usuarios',
  icon: 'nb-person',
  link: '/pages/usuarios',
  children: [
    {
      title: 'Usuarios Registrados',
      link: '/pages/usuarios',
    },
    {
      title: 'Crear Nuevo Usuario',
      link: '/pages/usuarios/crear',
    },
  ],
};

export { MENU_ITEMS, ADMIN_MENU_ITEM };

