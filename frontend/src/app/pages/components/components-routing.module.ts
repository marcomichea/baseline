import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsComponent } from './components.component';
import { PasswordComponent } from './password/password.component';
import { ProfileComponent } from './profile/profile.component';
import { UpdateProfileComponent } from './updateprofile/updateprofile.component';
import { UsersCreateOrEditComponent } from './users-create-or-edit/users-create-or-edit.component';
import { UsersComponent } from './users/users.component';
import { NewBaselineComponent } from './newBaseline/newBaseline.component';
import { BaselinesComponent } from './Baselines/baselines.component';


const routes: Routes = [{
  path: '',
  component: ComponentsComponent,
  children: [
    {
      path: 'perfil',
      component: ProfileComponent,
    },    
    {
      path: 'contraseña',
      component: PasswordComponent,
    },
    {
      path: 'actualizarperfil',
      component: UpdateProfileComponent,
    },
    {
      path: 'newBaseline',
      component: NewBaselineComponent,
    },
    {
      path: 'baselines',
      children: [
        {
          path: '',
          component: BaselinesComponent,
        },        
      ],
    },
    {
      path: 'usuarios',
      children: [
        {
          path: '',
          component: UsersComponent,

        },
        {
          path: 'editar/:userId',
          component: UsersCreateOrEditComponent,
        },
        {
          path: 'crear',
          component: UsersCreateOrEditComponent,
        },
      ],
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComponentsRoutingModule { }

export const routedComponents = [
  ComponentsComponent,
  ProfileComponent, 
  PasswordComponent,
  UpdateProfileComponent,  
  UsersComponent,
  UsersCreateOrEditComponent,  
  NewBaselineComponent,
  BaselinesComponent,
];
