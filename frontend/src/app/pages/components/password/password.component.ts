import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PasswordReset } from '../../../models/password-reset';
import { ProfileService } from '../../../services/profile.service';

@Component({
  selector: 'ngx-password',
  templateUrl: './password.component.html',
})
export class PasswordComponent  implements OnInit {

  public myform: FormGroup;
  public errorAlert =  {
    status : false,
    message : '',
  };
  public successAlert = {
    status : false,
    message : '',
  };
  public crendentials: PasswordReset;
  public submitted: boolean;
  public errors: any = [];

  constructor(private profileService: ProfileService,
    private ref: ChangeDetectorRef) {}

    ngOnInit() {
      this.myform = new FormGroup({
        password: new FormControl('', [
            Validators.required,
        ]),
        newpassword: new FormControl('', [
            Validators.minLength(6),
            Validators.maxLength(10),
            Validators.required,
        ]),
        repassword: new FormControl('', [
          Validators.minLength(6),
          Validators.maxLength(10),
          Validators.required,
        ]),
        language: new FormControl(),
      });
      this.crendentials = new PasswordReset();
      this.submitted = true;
    }

    public onSubmitPasswordChange() {
      const val = this.myform.value;
      if (val.password && val.repassword && val.newpassword && (val.repassword === val.newpassword)) {
        this.crendentials.password = val.password;
        this.crendentials.new_password = val.newpassword;
        this.crendentials.new_password_confirmation = val.repassword;
        this.sendChanges();
      } else {
        this.errorAlert.status = true;
        this.successAlert.status = false;
        this.submitted = false;
        this.errorAlert.message = 'Las contraseñas no coinciden.';
      }
      this.ref.detectChanges();
    }

    private sendChanges() {
      this.errors = [];
      this.profileService.changePassword(this.crendentials).subscribe(response => {
        this.errorAlert.status = false;
        this.successAlert.status = true;
        this.successAlert.message = 'Contraseña actualizada correctamente.';
        this.myform.reset();
        setTimeout(function(){
          history.back();
        }, 2000);
      }, error => {
        const res = error.error;
        if (res.error) {
          this.errors = Object.keys(res.error).map(key => (res.error[key][0]));
          this.errorAlert.message = '';
        } else {
          this.errorAlert.message = 'Ocurrió un error cambiando la contraseña.';
        }
        this.successAlert.status = false;
        this.errorAlert.status = true;
        this.ref.detectChanges();
      });
    }
}
