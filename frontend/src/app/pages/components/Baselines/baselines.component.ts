import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastStatus } from '../../../models/filters-params';
import { ToastrService } from 'ngx-toastr';
import { UsersService} from '../../../services/users.service';
import { DashboardService } from '../../../services/dashboard.service';


@Component({
  selector: 'ngx-baselines',
  templateUrl: './baselines.component.html',
})

export class BaselinesComponent implements OnInit {

    totalRecords: number;
    observer: any;       
    baselinesList: any[];
    userName: string;
  
 settings = {    
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    actions: {
      columnTitle: 'Editar/Ver',
      add: false,
      delete: false,
      position: 'right',
    },
    columns: {
        baseline_name: {
          title: 'Nombre Baseline',
          editable: true,
        },
        vendor: {
          title: 'Vendor',
          editable: false,
        },
        technology: {
          title: 'Tecnología',
          editable: false,
        },
        release: {
          title: 'Release',
          editable: false,
        },
        description: {
          title: 'Descripción',
          editable: true,
        },
        creation_date: {
          title: 'Fecha Creación',
          editable: false,
        },
        last_update: {
          title: 'Última Actualización',
          editable: false,
        },
        user_name:{
          title: 'Nombre Usuario',
          editable: false,
        },
      },
    noDataMessage: `No se encontraron datos`,
    rowClassFunction: (row) => {
      if (row.data.current_value !== row.data.new_value) {
        return 'editedRow';
      } else {
        return '';
      }
    },
  };

    constructor(
        private userService: UsersService,        
        private dashboardService: DashboardService,       
        private router: Router,
        private toastr: ToastrService) {}

    ngOnInit(){    
      this.fillTable();
      this.bindHorizontalScroll();
      this.addHorizontalTopScroll();      
    }
    
    onRowSelect(event): void{
       this.router.navigate([`/pages/dashboard/${event.data.baseline_name}`]);    
    }
    
    fillTable(){
          //Se carga el listado de los baselines guardados en la base de datos
      this.dashboardService.getBaselinesList().subscribe((res: any)=> {
        
        this.baselinesList = res;
        this.totalRecords = this.baselinesList.length;        
        
      });
    }

    saveChanges(event: any, multi: boolean = false) {
        
        const data = {
          details: (multi) ? event : [event.newData],
        };
        
        if (event.data.new_value === event.newData.new_value) {
          this.updateChangeTable(event);
        } 
      }

      updateChangeTable(event) {
        
        const param = event.newData;        
        this.dashboardService.saveChangesBaselines(param.description, param.baseline_name, param.id_baseline)
        .subscribe(response =>{
    
          this.showToast(ToastStatus.success, `Registro guardado exitosamente`);
          this.fillTable();
          event.confirm.resolve();    
          
        }, err => {
          this.showToast(ToastStatus.error, `Ocurrio un error guardando los datos`);
        });   
        
      }

    bindHorizontalScroll() {
        const wrapperTop = document.getElementById('wrapper1');
        const tableWrapper = document.getElementById('wrapper2');
        wrapperTop.onscroll = (evt) => {
          tableWrapper.scrollLeft = wrapperTop.scrollLeft;
        };
    
        tableWrapper.onscroll = (evt) => {
          wrapperTop.scrollLeft = tableWrapper.scrollLeft;
        };
      }
    
    addHorizontalTopScroll() {
        let topContainer: any = null;
        this.observer = new MutationObserver(function (mutations) {
          const tmp = document.querySelector('#baselinesTable table');
          if (tmp.clientWidth) {
            topContainer = (topContainer) ? topContainer : document.getElementById('topScroll');
            topContainer.style.width = tmp.clientWidth + 'px';
          }
        });
        this.observer.observe(document.getElementById('wrapper2'), { subtree: true, childList: true });
    }

    showToast(status, message) {
        const icon = status;
        this.toastr.show('', message, {}, status);
      }
    
}