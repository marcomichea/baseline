import { NgModule } from '@angular/core';
import { NbListModule,NbAlertModule, NbCardModule, NbDialogModule, NbPopoverModule, NbSpinnerModule } from '@nebular/theme';
import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';
import { Ng2Rut, RutValidator } from 'ng2-rut';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxPaginationModule, PaginatePipe } from 'ngx-pagination';
import { ThemeModule } from '../../@theme/theme.module';
import { ComponentsRoutingModule, routedComponents } from './components-routing.module';
import { UserTypeRendererComponent } from './users/user-type-renderer.component';

@NgModule({
  imports: [
    ThemeModule,
    ComponentsRoutingModule,
    TreeModule,
    Ng2SmartTableModule,
    NbAlertModule,
    ToasterModule.forRoot(),
    NbDialogModule.forRoot(),
    NgxPaginationModule,
    NbCardModule,
    NbSpinnerModule,
    Ng2Rut,
    NbPopoverModule,
    NbListModule,
  ],
  entryComponents: [
    UserTypeRendererComponent,
  ],
  declarations: [
    ...routedComponents,
    UserTypeRendererComponent,
  ],
  providers: [
    PaginatePipe,
    RutValidator,
  ],
})
export class ComponentsModule { }
