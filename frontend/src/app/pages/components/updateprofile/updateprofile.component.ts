import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RutValidator } from 'ng2-rut';
import { User } from '../../../models/user';
import { AuthService } from '../../../services/auth.service';
import { ProfileService } from '../../../services/profile.service';

@Component({
  selector: 'ngx-updateprofile',
  templateUrl: './updateprofile.component.html',
})
export class UpdateProfileComponent implements OnInit  {

  public myform: FormGroup;
  public userInfo: User;
  public errorAlert =  {
    status : false,
    message : [],
  };

  public successAlert = {
    status : false,
    message : '',
  };
  submitted: boolean;

  constructor(private authService: AuthService,
    private profileService: ProfileService,
    private  rv: RutValidator,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef) {

     }

  ngOnInit() {
    this.userInfo = this.authService.getUser();
    this.myform = this.fb.group({
      email: new FormControl(this.userInfo.email, [
          Validators.required,
          Validators.pattern('[^ @]*@[^ @]*'),
      ]),
      name: new FormControl(this.userInfo.name, [
          Validators.minLength(3),
          Validators.required,
      ]),
      last_name: new FormControl(this.userInfo.last_name, [
        Validators.minLength(3),
        Validators.required,
      ]),
      phone: new FormControl(this.userInfo.phone_number, [
        Validators.minLength(8),
        Validators.pattern('^[0-9]+$'),
      ]),
      mobile: new FormControl(this.userInfo.mobile_number, [
        Validators.minLength(8),
        Validators.pattern('^[0-9]+$'),
      ]),
      rut: [this.userInfo.rut, [Validators.required, this.rv]],
      profile: new FormControl({value: (this.userInfo.is_admin === 1 ? 'Administrador' : 'Usuario'),
      disabled: true}, Validators.required),
      language: new FormControl(),
    });

      this.submitted = true;
  }

  public onSubmitProfile() {
    const val = this.myform.value;
    if (this.myform.valid) {
      this.userInfo.email = val.email;
      this.userInfo.name = val.name;
      this.userInfo.last_name = val.last_name;
      this.userInfo.phone_number = val.phone;
      this.userInfo.mobile_number = val.mobile;
      this.userInfo.rut = val.rut;
      this.errorAlert.status = false;
      this.successAlert.status = false;
      this.sendChanges();
      this.submitted = true;
    } else {
      this.errorAlert.status = true;
      this.successAlert.status = false;
      this.errorAlert.message = ['Hay datos inválidos, verifique el formulario.'];
      this.submitted = false;
    }
    this.ref.detectChanges();
  }
  private sendChanges() {
    this.profileService.update(this.userInfo).subscribe(response =>  {
      if (response.hasOwnProperty('success')) {
        this.authService.setUser(this.userInfo);
        this.errorAlert.status = false;
        this.successAlert.status = true;
        this.successAlert.message = 'Datos actualizados correctamente.';
      } else {
        let errores = '';
        Object.keys(response).forEach(key => errores += response[key][0]);
        this.successAlert.status = false;
        this.errorAlert.status = true;
        this.errorAlert.message = ['Ocurrió un error actualizando.'];
        this.ref.detectChanges();
      }
      }, response => {
        const errorMsgs = [];
        const errors = response.error.error;
        if (errors) {
          // tslint:disable-next-line:forin
          for (const i in errors) {
            errorMsgs.push(errors[i][0]);
          }
        }
        this.successAlert.status = false;
        this.errorAlert.status = true;
        this.errorAlert.message = errorMsgs;
        this.ref.detectChanges();
      });

  }
}
