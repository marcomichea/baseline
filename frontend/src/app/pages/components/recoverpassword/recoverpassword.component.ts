import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'ngx-recoverpassword',
  templateUrl: './recoverpassword.component.html',
})
export class RecoverPasswordComponent implements OnInit {

  public myform: FormGroup;
  public errorAlert =  {
    status : false,
    message : '',
  };
  public successAuth = {
    status : false,
    message : '',
  };
  submitted: boolean;
  disableButton: boolean = false;

  constructor(private authService: AuthService, private ref: ChangeDetectorRef) {}

  ngOnInit() {
    this.myform = new FormGroup({
      email: new FormControl('', [
          Validators.required,
          Validators.pattern('[^ @]*@[^ @]*'),
      ]),
      language: new FormControl(),
    });
    this.submitted = true;
  }

  public onSubmitPasswordReset() {
    const val = this.myform.value;
    if (val.email) {
      this.sendResetPassword(val.email);
    } else {
      this.submitted = false;
    }
    this.ref.detectChanges();
  }

  public sendResetPassword(email: string) {
    this.errorAlert.status = false;
    this.successAuth.status = false;
    this.submitted = true;
    this.disableButton = true;
    this.authService.resetPassword(email).subscribe((response: any) => {
      this.errorAlert.status = false;
      this.successAuth.status = true;
      this.successAuth.message = 'Su contraseña fue renovada correctamente,' +
      ' Por favor verifique su correo electrónico.';
      this.ref.detectChanges();
      setTimeout(function () {
        location.href = response.url;
      }, 2000);
    }, err => {
      const errors = err.error.error;
      if (errors.hasOwnProperty('email')) {
        this.errorAlert.message = errors['email'][0];
      } else {
        this.errorAlert.message = 'Ocurrió un error desconocido.';
      }
      this.errorAlert.status = true;
      this.successAuth.status = false;
      this.disableButton = false;
    });
  }

}
