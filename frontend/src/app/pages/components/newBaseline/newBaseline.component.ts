import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FormMode } from '../../../form/enums/form-mode.enum';
import { Router } from '@angular/router';
import { ToastStatus } from '../../../models/filters-params';
import { markAllControlsAsTouch } from '../../../form/form-utils';
import { Baseline } from '../../../models/baseline';

import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../../services/auth.service';
import { DashboardService } from '../../../services/dashboard.service';

@Component({
  selector: 'ngx-newBaseline',
  templateUrl: './newBaseline.component.html',
})
export class NewBaselineComponent implements OnInit {

  public user: User;
  private _templates: any = null;
  private _idTemplate: any = null;

   // Listado de Templates
  templateList: any[];

  //Form
  mode: FormMode;
  form: FormGroup;

  constructor(
      private authService: AuthService,
      private dashboardService: DashboardService,
      private fb: FormBuilder,
      private router: Router,
      private toastr: ToastrService) {}


      tech : string;
      vendor: string;
      description: string;
      release: string;
      fCreation: string;
      fLastUpdate: string;
      baselineName: string;

    get templates() {
      return this._templates;
    }
    set templates(val) {
      if (this._templates !== val) {
        this._templates = val;
  
        if (this._templates !== null) {
          this.idTemplate = val.id_baseline;
          this.tech = val.technology;
          this.vendor = val.vendor;
          this.release = val.release;
          this.description = val.description;
          this.fCreation = val.creation_date;
          this.fLastUpdate = val.last_update;
          this.baselineName= val.baseline_name;          
        }
      }
    }
    
    get idTemplate() {
      return this._idTemplate;
    }
  
    set idTemplate(val) {
      if (this._idTemplate !== val) {
        this._idTemplate = val;        
      }
    }

    ngOnInit(): void {
        this.user = this.authService.getUser();
    
        console.log(this.user);
        //Se carga el listado de los Templates guardados en la base de datos
        this.dashboardService.getTemplateList().subscribe((res: any)=> {
            console.log(res);
            this.templateList = res;            
          });
        this.createForm();

    } 
      
    createBaseline(){
      // valida datos de formulario
      if (! this.form.valid) {
        this.showToasterErrorMessage('Existen errores en los campos del formulario. '),
        markAllControlsAsTouch(this.form);
        return;
      }
      
      const baseline = this.form.getRawValue();
      
      this.dashboardService.saveNewBaseline(this.idTemplate,baseline.baselineName ,
      this.vendor,this.tech,this.release,baseline.description,this.user.id.toString(), 
      this.user.name)
      .subscribe((res: any)=> {                            
          this.showToast(ToastStatus.success, res.success);            
          this.router.navigate([`/pages/dashboard/${baseline.baselineName}`]);                           
      });  

    }

    createForm(baseline?: Baseline) {
      baseline = baseline ? baseline : new Baseline();
  
      const fields: any = {
        baselineName: new FormControl(baseline.baseline_name || '', [
          Validators.minLength(3),
          Validators.maxLength(255),          
          Validators.required,
        ]),
        description: new FormControl(baseline.description || '', [
          Validators.minLength(3),
          Validators.maxLength(255),          
          Validators.required,
        ]),
      };
        
      this.form = this.fb.group(fields);
    }

    showToasterErrorMessage(message: string) {
      this.toastr.error(message, 'Error');
    }

    showToast(status, message) {
      const icon = status;
      this.toastr.show('', message, {}, status);
    }
}
