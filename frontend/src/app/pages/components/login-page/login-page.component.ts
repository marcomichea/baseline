import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Login } from '../../../models/login';
import { User } from '../../../models/user';
import { AuthService } from '../../../services/auth.service';


@Component({
  selector: 'ngx-login-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {

  public user: Login;
  public myform: FormGroup;
  public userInfo: User;
  public errorAlert =  {
    status : false,
    message : '',
  };
  public successAuth = {
    status : false,
    message : '',
  };
  returnUrl: string;
  submitted: boolean;

  constructor(private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private ref: ChangeDetectorRef) {}

  ngOnInit() {
    this.myform = new FormGroup({
      email: new FormControl('', [
          Validators.required,
          Validators.pattern('[^ @]*@[^ @]*'),
      ]),
      password: new FormControl('', [
          Validators.minLength(6),
          Validators.required,
      ]),
      language: new FormControl(),
    });
    this.user = new Login();
    this.userInfo = new User();
    this.authService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.submitted = true;
  }

  public onSubmitLogin(): void {
    const val = this.myform.value;
    if (val.email && val.password) {
      this.user.email = val.email;
      this.user.password = val.password;
      this.errorAlert.status = false;
      this.successAuth.status = false;
      this.login();
      this.submitted = true;
    } else {
      this.submitted = false;
    }
  }

  private login() {
    this.authService.login(this.user).subscribe(authentication => {
      if (authentication.token !== undefined) {
        this.successAuth.status = true;
        this.authService.setToken(authentication.token);
        this.successAuth.status = true;
        this.successAuth.message = 'Obteniendo datos de usuario, espere un momento...';
        this.getUserInfo();
      } else {
        this.errorAlert.status = true;
        this.errorAlert.message = 'Ocurrió un problema con token.';
      }
      this.ref.detectChanges();
    }, error => {
      this.errorAlert.status = true;
      if (error.statusText === 'Unauthorized') {
        this.errorAlert.message = 'Credenciales inválidas.';
      } if (error.hasOwnProperty('error')) {
        const msg = error['error']['error'];
        msg === 'invalid_credentials' ? 'Credenciales Inválidas' : msg;
        this.errorAlert.message = !msg ? 'Error en el servidor.' : msg ;
      } else {
        this.errorAlert.message = 'Error en el servidor.';
      }
      this.ref.detectChanges();
    });
  }

  private getUserInfo() {
    this.errorAlert.status = false;
    this.authService.getUserInfo().subscribe(userData => {
      this.userInfo = userData;
      this.authService.setUser(this.userInfo);
      this.successAuth.status = true;
      this.successAuth.message = 'Bienvenido(a): ' + this.userInfo.name;
      this.ref.detectChanges();
      this.router.navigate(['/pages']);
    }, error => {
      this.successAuth.status = false;
      this.errorAlert.status = true;
      this.errorAlert.message = 'Ocurrio un error obteniendo datos de usuario.';
      this.ref.detectChanges();
    });
  }
}
