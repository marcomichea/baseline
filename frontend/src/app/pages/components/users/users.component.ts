import { Component, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ServerDataSource } from 'ng2-smart-table';
import { UsersService } from '../../../services/users.service';
import { UserTypeRendererComponent } from './user-type-renderer.component';


@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
})
export class UsersComponent {

  @ViewChild('infoDialog') infoDialog: TemplateRef<any>;

  usersDataSource: ServerDataSource;

  settings = {
    mode: 'external',
    actions: {
      add: false,
      position: 'right',
      columnTitle: 'Acciones',
    },
    columns: {
      name: {
        title: 'Nombre',
        editable: false,
      },
      last_name: {
        title: 'Apellido',
        editable: false,
      },
      email: {
        title: 'Email',
        editable: false,
      },
      type: {
        title: 'Tipo',
        editable: false,
        type: 'custom',
        renderComponent: UserTypeRendererComponent,
      },
      rut: {
        title: 'RUT',
        editable: false,
      },
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    hideSubHeader: true,
    noDataMessage: `No se encontraron datos`,
  };

  constructor(
    private router: Router,
    private usersService: UsersService,
    private dialogService: NbDialogService,
  ) {
    this.usersDataSource = this.usersService.getUsersDataSource();
  }

  goToEditPage(ev) {
    this.router.navigate(['usuarios/editar', ev.data.id]);
  }

  confirmDelete(ev: any, dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, {
      context: {
        user: ev.data,
      },
    });
  }

  delete(userId: number) {
    this.usersService.delete(userId).subscribe(
      () => {
        this.dialogService.open(this.infoDialog, {
          context: {
            message: 'Usuario eliminado exitosamente.',
          },
        });
        this.usersDataSource.refresh();
      },
      (error) => {
        let message = 'Ocurrió un error durante la eliminación del usuario.';
        if (error.error && error.error.code === 'auth/main-admin-is-undeletable') {
          message = 'No es posible eliminar al administrador principal';
        }

        this.dialogService.open(this.infoDialog, {
          context: { message },
        });
      },
    );
  }
}
