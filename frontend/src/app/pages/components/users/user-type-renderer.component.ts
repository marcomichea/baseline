import { Component, Input, OnInit } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';


@Component({
  template: `
    {{renderedValue}}
  `,
})
export class UserTypeRendererComponent implements ViewCell, OnInit {

  renderedValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  ngOnInit() {
    this.renderedValue = this.rowData.is_admin ? 'Administrator' : 'Usuario';
  }

}
