import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbDialogService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { RutValidator } from 'ng2-rut';
import { UserType } from '../../../enums/user-type.enum';
import { FormMode } from '../../../form/enums/form-mode.enum';
import { markAllControlsAsTouch } from '../../../form/form-utils';
import { AlphaSpacesValidator } from '../../../form/validators/alpha-spaces.validator';
import { MatchValidator } from '../../../form/validators/match.validator';
import { PhoneValidator } from '../../../form/validators/phone.validator';
import { User } from '../../../models/user';
import { UsersService } from '../../../services/users.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'ngx-users-create-or-edit',
  templateUrl: './users-create-or-edit.component.html',
})
export class UsersCreateOrEditComponent implements OnInit {

  @ViewChild('successDialog') successDialog: TemplateRef<any>;

  UserType = UserType;
  FormMode = FormMode;

  userId: number;
  mode: FormMode;
  form: FormGroup;

  loadErrorMessage: string;

  constructor(
    private dialogService: NbDialogService,
    private userService: UsersService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private  rv: RutValidator,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    const userId = this.route.snapshot.paramMap.get('userId');

    if (userId === null) {
      this.mode = FormMode.Create;
      this.createForm();
    } else {
      this.userId = Number.parseInt(userId);
      this.mode = FormMode.Edit;

      this.userService.get(this.userId).subscribe(
        user => this.createForm(user),
        error => {
          if (error.status === 404) {
            this.loadErrorMessage = 'El usuario no existe. ';
          } else {
            this.loadErrorMessage = 'Ocurrió un error al recuperar el usuario. ';
          }
        },
      );
    }
  }

  createForm(user?: User) {
    user = user ? user : new User();

    const fields: any = {
      name: new FormControl(user.name || '', [
        Validators.minLength(3),
        Validators.maxLength(255),
        AlphaSpacesValidator(),
        Validators.required,
      ]),
      last_name: new FormControl(user.last_name || '', [
        Validators.minLength(3),
        Validators.maxLength(255),
        AlphaSpacesValidator(),
        Validators.required,
      ]),
      rut: [user.rut, [Validators.required, this.rv]],
      type: new FormControl(user.type || UserType.User, [
        Validators.required,
      ]),
      phone_number: new FormControl(user.phone_number || '', [
        Validators.minLength(8),
        PhoneValidator(),
      ]),
      mobile_number: new FormControl(user.mobile_number || '', [
        Validators.minLength(8),
        PhoneValidator(),
      ]),
      email: new FormControl(user.email || '', [
        Validators.email,
        Validators.maxLength(255),
        Validators.required,
      ]),
      confirmEmail: new FormControl(user.email || '', [
        Validators.email,
        Validators.maxLength(255),
        Validators.required,
      ]),
    };

    const validators = [ MatchValidator('email', 'confirmEmail') ];

    if (this.mode === FormMode.Create) {
      fields.password = new FormControl('', [
        Validators.minLength(6),
        Validators.maxLength(20),
        Validators.required,
      ]);
      fields.confirmPassword = new FormControl('', [
        Validators.minLength(6),
        Validators.maxLength(20),
        Validators.required,
      ]);
      validators.push(MatchValidator('password', 'confirmPassword'));
    }

    this.form = this.fb.group(fields, validators);
  }

  save() {
    if (! this.form.valid) {
      this.showToasterErrorMessage('Existen errores en los campos del formulario. '),
      markAllControlsAsTouch(this.form);
      return;
    }

    const user = this.form.getRawValue();

    if (this.mode === FormMode.Edit) {
      this.userService.update(this.userId, user).subscribe(
        () => this.dialogService.open(this.successDialog),
        () => this.showToasterErrorMessage('Ocurrió un error al crear el usuario. '),
      );
    } else if (this.mode === FormMode.Create) {
      this.userService.create(user).subscribe(
        () => this.dialogService.open(this.successDialog),
        (err) => {
          if (err.error.errors) {
            const errors = [];
            // tslint:disable-next-line:forin
            for (const error in err.error.errors) {
              errors.push(err.error.errors[error]);
            }
            this.showToasterErrorMessage(errors.map(error => `● ${error}`).join('\n'));
          } else
            this.showToasterErrorMessage('Ocurrió un error al guardar el usuario. ');
        },
      );
    }
  }

  showToasterErrorMessage(message: string) {
    this.toastr.error(message, 'Error');
  }
}
