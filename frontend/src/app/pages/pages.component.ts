import { Component, OnInit } from '@angular/core';
import { UserType } from '../enums/user-type.enum';
import { AuthService } from '../services/auth.service';
import { ADMIN_MENU_ITEM, MENU_ITEMS } from './pages-menu';


@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent implements OnInit {

  menu = [];

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.menu = Object.assign([], MENU_ITEMS);
    const user = this.authService.getUser();
    if ('' + user.is_admin === UserType.Admin) {
      this.menu.push(ADMIN_MENU_ITEM);
    }
  }
}
