import { NgModule } from '@angular/core';
import { NbDatepickerModule } from '@nebular/theme';
import { ThemeModule } from '../@theme/theme.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { Ng2Rut } from 'ng2-rut';
import { ToastrModule } from 'ngx-toastr';


const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,    
    MiscellaneousModule,
    NbDatepickerModule.forRoot(),
    ToastrModule.forRoot(),
    Ng2Rut,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
})
export class PagesModule {
}
