
class IgnoreItems {
  values: any[];
  ignore: Function = () => false;

  get length() {
    return this.values.length;
  }

  constructor(values?: any[], validate?: boolean, ignore?: Function) {
    if (ignore) this.ignore = ignore;
    if (validate === false) {
      this.values = values || [];
    } else {
      this.clear().add(values || []);
    }
  }
  clear() {
    this.values = [];
    return this;
  }
  add(values: any[]) {
    if (values.length) {
      this.values = values.reduce((list: any[], next: any) => {
        if (!this.ignore(list, next)) list.push(next);
        return list;
      }, this.values);
    }
    return this;
  }
  push(value: any) {
    if (!this.ignore(this.values, value)) this.values.push(value);
    return this;
  }
  drop(values: any[]) {
    this.values = this.filter((value: any[]) => {
      return values.indexOf(value) < 0;
    });
    return this;
  }

  filter(fn, args?: any) {
    return this.values.filter(fn, args);
  }
  map(fn, args?: any) {
    return this.values.map(fn, args);
  }
  except(values: any[]) {
    return this.filter((value: any[]) => {
      return values.indexOf(value) < 0;
    });
  }
}

function unique(list: any[], validate?: boolean) {
  return new IgnoreItems(
    list,
    validate,
    // tslint:disable-next-line:no-shadowed-variable
    (list, value) => (value === null || list.indexOf(value) >= 0),
  );
}
export { IgnoreItems, unique };

