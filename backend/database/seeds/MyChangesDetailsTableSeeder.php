<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MyChangesDetailsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('my_changes_details')->insert([
            [
                'my_changes_id' => 1,
                'date'  => Carbon::parse('2000-01-01'),
                'tech'  => 'LTE',
                'mo'    => 'mo',
                'param' => 'param',
                'bsc'   => 'bsc',
                'rnc'   => 'rnc',
                'region' => 'region',
                'cluster' => 'cluster',
                'sitio' => 'sitio',
                'celda' => 'celda',
            ],
            [
                'my_changes_id' => 1,
                'date'  => Carbon::parse('2000-01-01'),
                'tech'  => '3G',
                'mo'    => 'mo',
                'param' => 'param',
                'bsc'   => 'bsc',
                'rnc'   => 'rnc',
                'region' => 'region',
                'cluster' => 'cluster',
                'sitio' => 'sitio',
                'celda' => 'celda',
            ],
            [
                'my_changes_id' => 1,
                'date'  => Carbon::parse('2000-01-01'),
                'tech'  => 'LTE',
                'mo'    => 'mo',
                'param' => 'param',
                'bsc'   => 'bsc',
                'rnc'   => 'rnc',
                'region' => 'region',
                'cluster' => 'cluster',
                'sitio' => 'sitio',
                'celda' => 'celda',
            ],
            [
                'my_changes_id' => 1,
                'date'  => Carbon::parse('2000-01-01'),
                'tech'  => 'LTE',
                'mo'    => 'mo',
                'param' => 'param',
                'bsc'   => 'bsc',
                'rnc'   => 'rnc',
                'region' => 'region',
                'cluster' => 'cluster',
                'sitio' => 'sitio',
                'celda' => 'celda',
            ]
        ]);
    }
}
