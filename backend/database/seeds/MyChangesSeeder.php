<?php

use App\DbMoIndex;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MyChangesSeeder extends Seeder
{
    const ELEM_RED_CELDA = 'celda';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $id = DB::table('my_changes')->insertGetId(
            [
                'user_id' => 1,
                'name' => $faker->name(),
                'status' => 'temporal',
            ]
        );


        for ($i = 0; $i < 5; $i++){

            $index = DB::connection('export_dr')
                ->table('DB_MO_Index')
                ->limit(1)->offset($faker->numberBetween(1, 1210))->first();

            $tech = $index->TECH;
            $mo = $index->MO;
            $table = $index->TABLE_NAME_DB;

            $fields[] = $tech == '2G' ? 'bsc' : ($tech == '3G' ? 'rnc' : 'region');
            $fields = array_merge($fields, ['cluster', 'sitio', 'celda']);

            $excludeColumns = config('app.excludeColumns');

            // Buscamos los registros del indice
            $indexList = DbMoIndex::where([
                'tech' => $tech,
                'mo' => $mo,
            ])->get();

            // Genero las columnas de Elementos de Red
            $elemsRed = [];
            $columnsElemRed = '';
            foreach ($indexList as $index){
                $alias = strtolower($index->ELEMENTO_DE_RED);
                $excludeColumns[] = $index->COLUMN_NAME_DB;

                if ($alias == 'celda_tdd' || $alias == 'celda_fdd') {
                    $alias = self::ELEM_RED_CELDA;
                    $elemsRed[$alias][] = $index->COLUMN_NAME_DB;

                    if (count($elemsRed[$alias]) == 1)
                        $columnsElemRed .= "__CELDA__ AS '$alias',";
                } else {
                    $elemsRed[$alias] = $index->COLUMN_NAME_DB;
                    $columnsElemRed .= "{$index->COLUMN_NAME_DB} AS '$alias',";
                }
            }

            $param = DB::connection('export_dr')
                ->table('information_schema.COLUMNS')
                ->select('COLUMN_NAME')
                ->where('TABLE_SCHEMA', 'export_de_red')
                ->where('TABLE_NAME', $table)
                ->whereNotIn('COLUMN_NAME', $excludeColumns)
                ->first()->COLUMN_NAME;

            // Si existe 'celda' como elemento de red reemplazamos el marcador __CELDA__
            // por el COALESCE de cada columna
            if (isset($elemsRed[self::ELEM_RED_CELDA]) && count($elemsRed[self::ELEM_RED_CELDA]) > 0) {
                $columns = implode(',', $elemsRed[self::ELEM_RED_CELDA]);
                $elemsRed[self::ELEM_RED_CELDA] = "COALESCE($columns)";
                $columnsElemRed = str_replace('__CELDA__', $elemsRed[self::ELEM_RED_CELDA], $columnsElemRed);
            }

            $qry = DB::connection('export_dr')
                ->table($table)
                ->selectRaw("DATE(varDateTime) AS date, $columnsElemRed $param AS current_value")
                ->where(DB::raw('DATE(varDateTime)'), '2018-10-08');

            $total = $qry->count();

            for ($j = 0; $j < 10; $j++) {

                $row = $qry->limit(1)
                    ->offset($faker->numberBetween(1, $total))
                    ->first();

                $cols = [];
                foreach ($elemsRed as $name => $column) {
                    $cols[$name] = $row->{$name};
                }

                DB::table('my_changes_details')->insert(
                    array_merge([
                        'my_changes_id' => $id,
                        'date' => $row->date,
                        'tech' => $tech,
                        'mo' => $mo,
                        'param' => $param,
                        'current_value' => $row->current_value,
                        'new_value' => (string) (is_numeric($row->current_value) ? $faker->numberBetween() : str_random()),
                    ], $cols)
                );
            }
        }
    }
}
