<?php

use Illuminate\Database\Seeder;

class MyChangesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('my_changes')->insert([
            [
                'name' => 'Cambios #1',
                'description' => 'Descripción 1',
                'status' => 'temporal',
                'user_id' => 1,
            ],
            [
                'name' => 'Cambios #2',
                'description' => 'Descripción 2',
                'status' => 'temporal',
                'user_id' => 1,
            ],
            [
                'name' => 'Cambios #3',
                'description' => 'Descripción 3',
                'status' => 'temporal',
                'user_id' => 1,
            ],
            [
                'name' => 'Cambios #4',
                'description' => 'Descripción 4',
                'status' => 'temporal',
                'user_id' => 1,
            ],
            [
                'name' => 'Cambios #5',
                'description' => 'Descripción 5',
                'status' => 'temporal',
                'user_id' => 1,
            ],[
                'name' => 'Cambios #6',
                'description' => 'Descripción 6',
                'status' => 'temporal',
                'user_id' => 1,
            ],
            [
                'name' => 'Cambios #7',
                'description' => 'Descripción 7',
                'status' => 'temporal',
                'user_id' => 1,
            ],
            [
                'name' => 'Cambios #8',
                'description' => 'Descripción 8',
                'status' => 'temporal',
                'user_id' => 1,
            ],
            [
                'name' => 'Cambios #9',
                'description' => 'Descripción 9',
                'status' => 'temporal',
                'user_id' => 1,
            ],
            [
                'name' => 'Cambios #10',
                'description' => 'Descripción 10',
                'status' => 'temporal',
                'user_id' => 1,
            ],
            [
                'name' => 'Cambios #11',
                'description' => 'Descripción 11',
                'status' => 'temporal',
                'user_id' => 1,
            ],
            [
                'name' => 'Cambios #12',
                'description' => 'Descripción 12',
                'status' => 'temporal',
                'user_id' => 1,
            ]
        ]);
    }
}
