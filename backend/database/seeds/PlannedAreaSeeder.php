<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlannedAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $tech = $faker->randomElement(['2G','3G','LTE']);
        $rows = [
            '2G' => 11917,
            '3G' => 39826,
            'LTE' => 20000,
        ];
        $fields[] = $tech == '2G' ? 'bsc' : ($tech == '3G' ? 'rnc' : 'region');
        $fields = array_merge($fields, ['cluster', 'sitio', 'celda']);

        $id = DB::table('planned_areas')->insertGetId(
            [
                'user_id' => 1,
                'name' => 'AreaCali_'.$tech,
                'tech' => $tech,
                'net_type' => $faker->randomElement($fields),
                'net_elements' => '',
            ]
        );

        for ($i = 0; $i < 10; $i++){
            $record = DB::connection('export_dr')
                ->table('ListadoNacional_'.$tech)
                ->limit(1)->offset($faker->numberBetween(1, $rows[$tech]))->first();

            $attribute = $tech == 'LTE' ? 'Region' : strtoupper($fields[0]);

            DB::table('planned_area_cells')->insert(
                [
                    'planned_area_id' => $id,
                    $fields[0] => $record->{$attribute},
                    'cluster' => $record->CLUSTER,
                    'sitio' => $record->SITIO,
                    'celda' => $record->CELDA,
                ]
            );
        }
    }
}
