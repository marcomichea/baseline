/*
# Innova4J - Dario Romero <dromero@innova4j.com>
# Config to connect edr_users table with export_de_red
# Add this to MySQL configuration
# Section [mysqld]
federated
#
#
*/

-- CREATE DATABASE edr_users;
-- GRANT ALL PRIVILEGES ON edr_users.* to 'edr_users'@'%' ;
-- GRANT ALL PRIVILEGES ON edr_users.* 'edr_users'@'localhost' ;
-- FLUSH PRIVILEGES;
-- use edr_users;

DROP TABLE `planned_area_cells` ;
CREATE TABLE `planned_area_cells` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `planned_area_id` int(10) unsigned NOT NULL,
  `bsc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rnc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cluster` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celda` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `planned_area_cells_planned_area_id_foreign` (`planned_area_id`),
  CONSTRAINT `planned_area_cells_planned_area_id_foreign` FOREIGN KEY (`planned_area_id`) REFERENCES `planned_areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=FEDERATED
  CONNECTION='mysql://__DB_USERNAME__:__DB_PASSWORD__@__DB_HOST__:__DB_PORT__/__DB_DATABASE__/planned_area_cells';


DROP TABLE `my_changes_details` ;
CREATE TABLE `my_changes_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `tech` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bsc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rnc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cluster` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sitio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `celda` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `set` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ldn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `param` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `my_changes_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `my_changes_details_my_changes_id_foreign` (`my_changes_id`),
  CONSTRAINT `my_changes_details_my_changes_id_foreign` FOREIGN KEY (`my_changes_id`) REFERENCES `my_changes` (`id`) ON DELETE CASCADE
) ENGINE=FEDERATED
  CONNECTION='mysql://__DB_USERNAME__:__DB_PASSWORD__@__DB_HOST__:__DB_PORT__/__DB_DATABASE__/my_changes_details';



#EOTMPL
