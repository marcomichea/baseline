<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLdnAndSetFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('my_changes_details', function (Blueprint $table) {
            $table->string('set')->after('celda')->nullable();
            $table->string('ldn')->after('set')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('my_changes_details', function (Blueprint $table) {
            $table->dropColumn(['set', 'ldn']);
        });
    }
}
