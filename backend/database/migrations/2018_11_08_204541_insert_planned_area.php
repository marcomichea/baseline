<?php

use App\PlannedArea;
use App\PlannedAreaCell;
use Illuminate\Database\Migrations\Migration;

class InsertPlannedArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        return; // comentar esta linea para agregar seeder
        $faker = Faker\Factory::create();
        $rows = \App\Http\Controllers\PlannedAreaController::$rows;
        for ($i = 0; $i < 5; $i++){
            $tech = $faker->randomElement(array_keys($rows));
            $plannedArea = PlannedArea::create([
                'user_id' => 1,
                'name' => 'Area Random '.$tech,
                'tech' => $tech,
                'net_type' => $faker->randomElement($rows[$tech]),
                'net_elements' => $faker->word,
            ]);
            $cells = [];
            for ($j = 0; $j < 10; $j++){
                $cellRows = [];
                foreach ($rows[$tech] as $row) {
                    $cellRows[$row] = $faker->word;
                }
                $cells[]= new PlannedAreaCell($cellRows);
            }
            $plannedArea->cells()->saveMany($cells);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
