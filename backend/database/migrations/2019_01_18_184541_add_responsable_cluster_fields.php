<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResponsableClusterFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planned_area_cells', function (Blueprint $table) {
            $table->string('responsable_cluster')->after('celda')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planned_area_cells', function (Blueprint $table) {
            $table->dropColumn(['responsable_cluster']);
        });
    }
}
