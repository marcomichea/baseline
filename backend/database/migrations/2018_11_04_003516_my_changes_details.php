<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MyChangesDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_changes_details', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('tech');
            $table->string('mo');
            $table->string('bsc')->nullable();
            $table->string('rnc')->nullable();
            $table->string('region')->nullable();
            $table->string('cluster')->nullable();
            $table->string('sitio')->nullable();
            $table->string('celda')->nullable();
            $table->string('param');
            $table->string('current_value')->nullable();
            $table->string('new_value')->nullable();
            $table->unsignedInteger('my_changes_id');
            $table->timestamps();

            $table->foreign('my_changes_id')->references('id')->on('my_changes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_changes_details');
    }
}
