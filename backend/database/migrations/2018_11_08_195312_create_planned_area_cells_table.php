<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlannedAreaCellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planned_area_cells', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('planned_area_id');
            $table->string('bsc')->nullable();
            $table->string('rnc')->nullable();
            $table->string('region')->nullable();
            $table->string('cluster');
            $table->string('sitio');
            $table->string('celda');
            $table->timestamps();

            $table->foreign('planned_area_id')
                ->references('id')->on('planned_areas')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planned_area_cells');
    }
}
