<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Original de Laravel
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

// Iniciar sesión
Route::post('user/login', 'APILoginController@login');

// Recuperar contraseña
Route::post('user/password/reset', 'APILoginController@sendRecoverPassword');

// Refrescar token
Route::get('token/refresh', 'APILoginController@refreshToken');

// Grupo de rutas protegidas
Route::middleware(['jwt.auth'])->group(function () {

    /**
     * Endpoints para administración de usuarios
     */
    // Obtener datos del usuario autenticado
    Route::get('user', function (Request $request) {
        return auth()->user();
    });

    // Cambiar contraseña del usuario actual
    Route::put('user/password', 'APILoginController@changePassword');

    // Actualizar perfil del usuario actual
    Route::put('user', 'APILoginController@updateUserProfile');

    Route::get('users', 'UserController@getAll');

    Route::get('users/{id}', 'UserController@get');

    Route::post('users', 'UserController@create');

    Route::put('users/{id}', 'UserController@update');

    Route::delete('users/{id}', 'UserController@delete');

    /**
     * Endpoints para el Dashboard
     */

    // Graba los parametros editados por Mo
    Route::get('save/changes/paramMo', 'DashboardController@saveChangesParamMo');

    // Graba los parametros editados por Mo
    Route::get('save/changes/baselines', 'DashboardController@saveChangesBaselines');    

    // Graba nuevo baseline
    Route::get('save/newBaseline', 'DashboardController@saveNewBaseline');
    

    // Consultar lista de baselines
    Route::get('baseline/list', 'DashboardController@getBaselineList');

    // Consultar lista de templates
    Route::get('template/list', 'DashboardController@getTemplateList');

    // Consultar lista de tecnologías
    Route::get('tech/list', 'DashboardController@getTechList');

    // Consultar lista de raices para el arbol de la tecnología seleccionada
    Route::get('tech/tree', 'DashboardController@getTechTree');

    // Filtrar lista de MOs
    Route::get('mo/list', 'DashboardController@filterMOList');

    // Consultar lista de parametros según MO seleccionado
    Route::get('params/list', 'DashboardController@getParamsList');

    // Consultar lista de parametros según MO seleccionado
    Route::get('data/params/list', 'DashboardController@getDataParamsList');

    // Filtrar lista de Parámetros
    Route::get('params/filter/list', 'DashboardController@filterParamsList');

    // Consultar datos de la tabla principal
    Route::get('data', 'DashboardController@getData');

    // Consultar datos de la tabla principal
    Route::get('data/export', 'DashboardController@exportData');

    // Consultar cabecera de la tabla principal
    Route::get('data/headers', 'DashboardController@getDataHeaders');

    // Consultar frecuencia de datos
    Route::get('data/frequency', 'DashboardController@getDataFrequency');

    // Consultar fecha de última actualización de los datos
    Route::get('date', 'DashboardController@getDate');

    /**
     * Endpoints para el Dashboard de Hardware
     */
    // Consultar lista de tecnologías
    Route::get('hw/tech/list', 'HardwareController@getTechList');

    // Consultar lista de raices para el arbol de la tecnología seleccionada
    Route::get('hw/tech/tree', 'HardwareController@getTechTree');

    // Filtrar lista de MOs
    Route::get('hw/mo/list', 'HardwareController@filterMOList');

    // Consultar lista de parametros según MO seleccionado
    Route::get('hw/params/list', 'HardwareController@getParamsList');

    // Filtrar lista de Parámetros
    Route::get('hw/params/filter/list', 'HardwareController@filterParamsList');

    // Consultar datos de la tabla principal
    Route::get('hw/data', 'HardwareController@getData');

    // Exportar datos de la tabla principal
    Route::get('hw/data/export', 'HardwareController@exportData');

    // Consultar cabecera de la tabla principal
    Route::get('hw/data/headers', 'HardwareController@getDataHeaders');

    // Consultar frecuencia de datos
    Route::get('hw/data/frequency', 'HardwareController@getDataFrequency');

    // Consultar fecha de última actualización de los datos
    Route::get('hw/date', 'HardwareController@getDate');

    /**
     * Endpoints para Mis Cambios
     */
    // Guardar mis cambios
    Route::post('mychanges', 'MyChangesController@save');

    // Guardado masivo de mis cambios
    Route::post('mychanges/bulk', 'MyChangesController@saveMany');

    // Listado de mis cambios
    Route::get('mychanges/{id}', 'MyChangesController@getAttributes')
        ->where('id', '[0-9]+');

    // Listado de mis cambios
    Route::get('mychanges/list', 'MyChangesController@getList');

    // Obtener detalles de mis cambios
    Route::get('mychanges/details/{id}', 'MyChangesController@getDetails')
        ->where('id', '[0-9]+');

    // Actualizar registros de mis cambios
    Route::put('mychanges', 'MyChangesController@save');

    // Eliminar mis cambios
    Route::delete('mychanges', 'MyChangesController@delete');

    // Exportar Mis Cambios
    Route::get('mychanges/export/{id}', 'MyChangesController@export')
        ->where('id', '[0-9]+');

    // Eliminar un detalle de mis cambios
    Route::delete('mychanges/details/{id}', 'MyChangesController@deleteDetail')
        ->where('id', '[0-9]+');

    // Actualizar Valores Actuales de Mis Cambios
    Route::put('mychanges/update/{id}', 'MyChangesController@updateFromOriginalData')
        ->where('id', '[0-9]+');

    /**
     * Endpoints para Area Planificada
     */
    Route::resource('plannedareas', 'PlannedAreaController')->only([
        'index', 'store', 'show', 'update', 'destroy',
    ]);
    Route::resource('plannedareas/{id}/cells', 'PlannedAreaCellController')->only([
        'index', // 'store', 'show', 'update', 'destroy',
    ]);

    /**
     * Endpoints para Downloads
     */
    // Consultar datos de una descarga
    Route::get('download/{id}', 'DownloadsController@getDownloadById')
        ->where('id', '[0-9]+');

    // Consultar lista de descargas para el usuario actual en estatus 'pending' y 'ready'
    Route::get('download/list', 'DownloadsController@getDownloadsByUser');

    // Descargar archivo generado de una descarga
    Route::get('download/file/{id}', 'DownloadsController@getFile')
        ->where('id', '[0-9]+');

    // Grupo de rutas para base de datos export_dr
    Route::prefix('dr')->group(function(){
        // Obtener tecnologías
        Route::get('tech', 'ExportDrController@getTechList');
        // Obtener listado nacional
        Route::get('national/{type}/elements', 'ExportDrController@getNationalElements');
        // Fitrar listado nacional
        Route::get('national/{type}/cells', 'ExportDrController@getNationalCells');
    });
});
