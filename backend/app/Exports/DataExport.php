<?php

namespace App\Exports;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HardwareController;
use App\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithCustomChunkSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DataExport implements FromQuery, WithHeadings, WithMapping, WithCustomChunkSize
{
    use Exportable;

    public $user;

    public $requestParams;

    public $request;

    public $query;

    public $columns;

    public $fileName;

    public $userId;

    public $downloadId;

    public $controllerId;

    private $controller;

    /**
     * Obtiene la instancia del controlador correspondiente.
     *
     * @return DashboardController|HardwareController
     */
    public function getController()
    {
        if (!isset($this->controller)){
            if ($this->controllerId == 'DB')
                $this->controller = new DashboardController();
            else
                $this->controller = new HardwareController();
        }
        return $this->controller;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if (!isset($this->user)) {
            $this->user = User::find($this->userId);
        }
        return $this->user;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        if (!isset($this->request)) {
            $this->request = new Request($this->requestParams);
        }
        return $this->request;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        if (!isset($this->columns)) {
            $this->columns = $this->getController()->getDataHeaders($this->getRequest());
        }
        return $this->columns;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        if (!isset($this->query)) {
            $this->query = $this->getController()->buildQuery($this->getRequest(), true, $this->getUser())->orderBy('date');
        }
        return $this->query;
    }

    public function getMO()
    {
        return $this->getRequest()->mo;
    }

    public function getParam()
    {
        return $this->getRequest()->param;
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        $data = json_decode(json_encode($row), true);
        unset($data['date']);
        $data = array_prepend($data, $this->getMO());
        $currentValue = array_pop($data);
        array_push($data, $this->getParam(), $currentValue);

        return $data;
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 50000;
    }

    public function filtersRow()
    {
        $row[0] = $this->getDate();
        $row[1] = $this->getTech();
        $row[2] = $this->getMO();
        $row[3] = $this->getParam();

        return [implode(' / ', $row)];
    }

    public function getDate()
    {
        return $this->getRequest()->date ?: $this->getController()->getDate()['date'];
    }

    public function getTech()
    {
        return $this->getRequest()->tech;
    }

    public function getFilename()
    {
        if (!isset($this->fileName)) {
            $filename[0] = $this->getDate();
            $filename[1] = strtolower($this->getTech());
            $filename[2] = strtolower($this->getMO());
            $filename[3] = strtolower($this->getParam());
            $filename[4] = 'datos_tabla.csv';
            $this->fileName = implode('-', $filename);
        }
        return $this->fileName;
    }
}
