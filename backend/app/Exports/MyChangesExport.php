<?php

namespace App\Exports;

use App\MyChangeDetail;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class MyChangesExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStrictNullComparison
{
    use Exportable;

    /**
     * @var array Columnas a mostrar con sus nombres
     */
    protected $columns = [
        'Fecha' => 'date',
        'Tecnología' => 'tech',
        'MO' => 'mo',
        'Región' => 'region',
        'RNC' => 'rnc',
        'BSC' => 'bsc',
        // 'Cluster' => 'cluster',
        'Sitio' => 'sitio',
        'Celda' => 'celda',
        'SET' => 'set',
        'LDN' => 'ldn',
        'Parametro' => 'param',
        'Valor Actual' => 'current_value',
        'Valor Nuevo' => 'new_value',
    ];

    /**
     * @var integer Id del registro de Mis Cambios a exportar
     */
    private $id;

    /**
     * MyChangesExport constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return MyChangeDetail::select(array_values($this->columns))
            ->where('my_changes_id', $this->id)->get();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return array_keys($this->columns);
    }
}
