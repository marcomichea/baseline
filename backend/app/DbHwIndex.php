<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DbHwIndex extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'export_dr';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'DB_HW_INDEX';

    public function scopeTech($query, $select = null)
    {
        return $query->select('tech')->distinct()->orderBy('tech');
    }

    public function scopeMo($query, $mo, $tech)
    {
        $query->select('tech','mo')->distinct()
            ->where('mo', 'like', "%$mo%");

        if ($tech == '3G') {
            $query->where('tech', 'like', $tech.'%');
        } else {
            $query->where('tech', $tech);
        }

        return $query;
    }
}
