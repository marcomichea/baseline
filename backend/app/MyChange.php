<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MyChange extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'my_changes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','user_id'];

    /**
     * @var string Fecha de ultima modificación de los detalles
     */
    private $lastDataUpdatedDate;

    /**
     * @var string Fecha de ultima modificación del detalle o el maestro
     */
    private $updated_at;

    /**
     * User which this change belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Details of this change.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function details()
    {
        return $this->hasMany(MyChangeDetail::class, 'my_changes_id');
    }

    public function getDataUpdatedAtAttribute()
    {
        if (!isset($this->lastDataUpdatedDate)) {
            $this->lastDataUpdatedDate = $this->details()->max('updated_at');
        }
        return $this->lastDataUpdatedDate;
    }

    public function getUpdatedAtAttribute()
    {
        if (!isset($this->updated_at)) {
            $dataDate = Carbon::parse($this->getDataUpdatedAtAttribute());
            $myDate = Carbon::parse($this->attributes['updated_at']);

            $this->updated_at = $myDate->greaterThan($dataDate) ? $myDate->toDateTimeString() : $dataDate->toDateTimeString();
        }
        return $this->updated_at;
    }
}
