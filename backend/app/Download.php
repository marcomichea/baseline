<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static Download find($id, $columns = ['*'])
 *
 * Class Download
 * @package App
 */
class Download extends Model
{
    protected $table = 'downloads';

    protected $fillable = ['user_id','params'];
}
