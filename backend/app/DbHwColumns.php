<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DbHwColumns extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'export_dr';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'DB_HW_COLUMNS';
}
