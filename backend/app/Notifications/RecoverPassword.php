<?php

namespace App\Notifications;

use App\Mail\RecoverPassword as MailRecoverPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RecoverPassword extends Notification
{
    use Queueable;

    /**
     * @var User Usuario que está recuperando su contraseña.
     */
    public $user;
    /**
     * @var string Nueva contraseña.
     */
    public $pwd;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $pwd)
    {
        $this->user = $user;
        $this->pwd = $pwd;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailRecoverPassword())->subject(config('app.name').' - Recuperación de Contraseña')
                    ->greeting('Estimado(a) '.$this->user->full_name)
                    ->line('Su nueva clave de acceso al sistema es la siguiente:')
                    ->body('<b>'.$this->pwd.'</b>')
                    ->action('Entrar al Sistema', config('app.urls.login'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
