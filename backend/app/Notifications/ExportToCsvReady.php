<?php

namespace App\Notifications;

use App\Mail\ExportToCsvReady as MailExportToCsvReady;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class ExportToCsvReady extends Notification
{
    use Queueable;

    protected $file;

    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($file, $user)
    {
        $this->file = $file;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailExportToCsvReady())->subject(config('app.name').' - Datos Exportados')
            ->greeting('Estimado(a) '.$this->user->full_name)
            ->line('El archivo con los datos en formato CSV está adjunto a este correo.')
            ->attach($this->file);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
