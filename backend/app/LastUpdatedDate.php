<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LastUpdatedDate extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    // todo Uncomment when real table of dates is available
    protected $connection = 'export_dr';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'LastUpdatedDate';
}
