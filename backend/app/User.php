<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'phone_number', 'mobile_number', 'is_admin', 'email', 'password', 'rut'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Obtener el nombre completo del usuario.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->name .' '. $this->last_name;
    }

    /**
     * Changes of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mychanges()
    {
        return $this->hasMany(MyChange::class);
    }

    /**
     * Downloads of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function downloads()
    {
        return $this->hasMany(Download::class);
    }
}
