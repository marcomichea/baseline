<?php

namespace App\Mail;

use Illuminate\Notifications\Messages\MailMessage;

class RecoverPassword extends MailMessage
{
    /**
     * @var string Cuerpo del mensaje en html.
     */
    public $body;

    /**
     * Set html body.
     *
     * @param $value
     * @return $this
     */
    public function body($value)
    {
        $this->body = $value;

        return $this;
    }

    public function toArray()
    {
        return parent::toArray() + ['body' => $this->body];
    }
}
