<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlannedAreaCell extends Model
{
    protected $fillable = ['planned_area_id', 'cluster', 'sitio', 'celda', 'bsc', 'rnc', 'region',  'responsable_cluster'];

    /**
     * Get the planned area that owns the cell.
     */
    public function plannedArea()
    {
        return $this->belongsTo('App\PlannedArea');
    }
}
