<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DbPlanAreaIndex extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'export_dr';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'DB_PLAN_AREA_Index';

    public function scopeTech($query, $select = null)
    {
        return $query->select('tech')->distinct()->orderBy('tech');
    }
}
