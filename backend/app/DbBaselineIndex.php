<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DbBaselineIndex extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'export_dr';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Baseline';

    public function scopeTech($query, $select = null)
    {
        return $query->select('id_baseline','is_template','baseline_name','vendor','technology',
                            'release','description','creation_date','last_update','user_id','user_name','fromTemplate')
                     ->where('is_template','=','0');
    }

    public function scopeTechTemplate($query, $select = null)
    {
        return $query->select('id_baseline','baseline_name','vendor','technology',
                            'release','description','creation_date','last_update')
                     ->where('is_template','=','1');
    }
  
    
}