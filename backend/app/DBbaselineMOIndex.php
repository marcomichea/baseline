<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DbBaselineMOIndex extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'export_dr';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'BaselineMo';

    public function scopeTech($query, $select = null)
    {
        return $query->select('id_baseline','technology')->distinct()->orderBy('technology');                    
    }

    public function scopeMo($query, $mo, $tech){
        $query->select('technology','mo')->distinct()
        ->where('mo','like',"%$mo%");
        
        if($tech == '3G'){
            $query->where('technology','like', $tech.'%');
        }else{
            $query->where('technology', $tech);
        }

        return $query;
    }

    public function scopeTechTree($query, $select = null)
    {
        return $query->select('technology')->distinct()
                  ->where('id_baseline','=', '2');
        
    }

}
