<?php

namespace App\Jobs;

use App\Download;
use App\Exports\DataExport;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Concerns\WithMapping;

class InvalidDownload extends \Exception {}

class ExportDataToCsv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $filePath;

    /**
     * @var DataExport
     */
    public $export;

    protected $lote = 0;

    /**
     * @var int Tiempo en que debe expirar la tarea en segundos
     */
    public $timeout = 300;

    /**
     * @var int El número máximo de veces que se intentará procesar la tarea.
     */
    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(DataExport $dataExport)
    {
        $this->export = $dataExport;
    }

    /**
     * Execute the job.
     * @return void
     * @throws InvalidDownload
     */
    public function handle()
    {
        $exp = $this->export;
        $download = Download::find($exp->downloadId);
        if ($download && $download->status == 'pending') {
            $this->buildFile();
            $this->compressFile();
            $download->filename = $exp->getFilename();
            $download->status = 'ready';
            $download->save();
        } else {
            throw new InvalidDownload('No se encuentra la descarga o no está en estatus "pending".');
        }
    }

    /**
     * Construye y guarda el archivo csv
     */
    public function buildFile()
    {
        $exp = $this->export;
        // Hacemos el nombre del archivo único
        $fileName = str_replace('.csv', '', $exp->getFilename());
        do {
            $exp->fileName = $fileName . '_' . str_random(8) . '.csv';
            $this->filePath = storage_path('app') . DIRECTORY_SEPARATOR . $exp->fileName;
        } while (file_exists($this->filePath));

        // Creamos el archivo
        $file = fopen($this->filePath, 'a');
        ftruncate($file, 0);

        // Insertamos valores de filtro
        fputcsv($file, $exp->filtersRow());

        fputcsv($file, $exp->headings());

        // Insertamos los registros en el archivo por lotes
        $exp->query()->chunk($exp->chunkSize(), function ($records) use ($file, $exp) {
            $this->lote++;
            foreach ($records as $row) {
                if ($exp instanceof WithMapping) {
                    $row = $exp->map($row);
                }
                fputcsv($file, $row);
            }
        });
        fclose($file);
    }

    public function getContent()
    {
        $this->buildFile();

        $file = file_get_contents($this->filePath);

        // Eliminar archivo
        unlink($this->filePath);

        return $file;
    }

    /**
     * Comprime archivo CSV en ZIP y elimina el CSV
     */
    public function compressFile()
    {
        $this->export->fileName = str_replace('.csv', '.zip', $this->export->fileName);
        $zipFile = storage_path('app') . DIRECTORY_SEPARATOR . $this->export->fileName;
        Zipper::make($zipFile)->add($this->filePath)->close();
        unlink($this->filePath);
    }
}
