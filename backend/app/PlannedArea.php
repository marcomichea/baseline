<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlannedArea extends Model
{
    /**
     * The attributes that can be filled.
     *
     * @var array
     */
    protected $fillable = ['name', 'tech', 'net_type', 'net_elements'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'net_elements' => 'array',
    ];

    /**
     * Get the cells for the Planned Area.
     */
    public function cells()
    {
        return $this->hasMany('App\PlannedAreaCell');
    }
}
