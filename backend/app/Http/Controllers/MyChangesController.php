<?php

namespace App\Http\Controllers;

use App\DbMoIndex;
use App\Exports\MyChangesExport;
use App\MyChange;
use App\MyChangeDetail;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MyChangesController extends Controller
{
    const ELEM_RED_CELDA = 'celda';

    /**
     * Obtiene el temporal del usuario autenticado o crea uno nuevo si no existe.
     *
     * @return MyChange
     */
    public function getOrCreateTemporal()
    {
        /** @var User $user Usuario autenticado */
        $user = auth()->user();

        // Verifico si existe un temporal
        $mychange = $user->mychanges()->where('status', 'temporal')->first();

        // Si no existe un temporal creamos uno nuevo
        if (!$mychange) {
            $mychange = new MyChange([
                'name' => 'Temporal_' . date('Y-m-d'),
                'status' => 'temporal',
                'user_id' => $user->id
            ]);
        }

        return $mychange;
    }

    /**
     * Agrega los registros de detalles al temporal del usuario.
     * Si no existe temporal para el usuario se crea uno nuevo.
     * Si se indica un id de mis cambios se modificará el registro asociado.
     * Si se indica nombre y descripción se modificará y guardará como cambio definitivo.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        /** @var User $user Usuario autenticado */
        $user = auth()->user();

        // Validamos input
        $userIsMe = function ($query) use ($user) {
            return $query->where('user_id', $user->id);
        };

        $validationRules = [
            'id' => ['nullable','integer','min:1',
                Rule::exists((new MyChange())->getTable())->where($userIsMe)],
            'name' => ['nullable','string','max:255',
                Rule::unique((new MyChange())->getTable())->where($userIsMe)->ignore($request->get('id'))],
            'description' => 'nullable|string|max:255',
            'details' => 'nullable|array',
        ];
        $validator = Validator::make($request->all(), $validationRules, [], [
            'name' => 'Nombre',
            'description' => 'Descripción',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $id = $request->input('id');
        $name = $request->input('name');
        $description = $request->input('description');
        $details = $request->input('details');

        // Validamos detalles si existen
        if ($details) {
            $validationRules = [
                '*.date' => 'required|date',
                '*.tech' => ['required',Rule::in($this->getIndexTechList())],
                '*.mo' => 'required|string|max:255',
                '*.bsc' => 'nullable|string|max:255',
                '*.rnc' => 'nullable|string|max:255',
                '*.region' => 'nullable|string|max:255',
                '*.cluster' => 'nullable|string|max:255',
                '*.sitio' => 'nullable|string|max:255',
                '*.celda' => 'nullable|string|max:255',
                '*.set' => 'nullable|string|max:255',
                '*.ldn' => 'nullable|string|max:255',
                '*.param' => 'required|string|max:255',
                '*.current_value' => 'present|nullable|string|max:255',
                '*.new_value' => 'required|string|max:255',
            ];
            $validator = Validator::make($details, $validationRules, [], [
                '*.current_value' => 'Valor Actual',
                '*.new_value' => 'Valor Nuevo',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 400);
            }
        }

        DB::beginTransaction();

        try {
            $hasNoDetails = !is_array($details) || count($details) == 0;

            // Se valida que exista un nombre ó detalles que agregar o modificar
            if (!$name && $hasNoDetails){
                return response()->json(['error' => 'Debe indicar un nombre o listado de cambios para guardar'], 400);
            }

            // Si indicaron id consulto el registro
            if ($id) {
                $mychange = $user->mychanges()->where('id', $id)->first();
            } else {
                // Verifico si existe un temporal
                $mychange = $user->mychanges()->where('status', 'temporal')->first();

                // Si no existe un temporal creamos uno nuevo
                if (!$mychange) {
                    // Si hay name debe existir temporal
                    if ($name) {
                        return response()->json(['error' => 'No existen cambios temporales para guardar'], 400);
                    } else if ($hasNoDetails) {
                        return response()->json(['error' => 'Debe indicar listado de cambios para guardar'], 400);
                    } else {
                        $mychange = new MyChange([
                            'name' => 'Temporal_' . date('Y-m-d'),
                            'status' => 'temporal',
                            'user_id' => $user->id
                        ]);
                    }
                }
            }

            // Si indicaron Nombre del cambio es un guardado final
            if ($name) {
                $mychange->name = $name;
                $mychange->description = $description;
                $mychange->status = 'definitive';
            }

            if ($mychange->save()) {
                // Definimos los atributos para el update de los detalles
                $searchAttributes = ['date', 'tech', 'mo', 'bsc', 'rnc', 'region',
                    'cluster', 'sitio', 'celda', 'set', 'ldn', 'param','my_changes_id'];
                $delayedAttributes = ['current_value', 'new_value'];

                foreach ($details as $data) {
                    $data['my_changes_id'] = $mychange->id;

                    $searchData = array_only($data, $searchAttributes);
                    $delayedData = array_only($data, $delayedAttributes);

                    MyChangeDetail::updateOrCreate($searchData, $delayedData);
                }

                DB::commit();

                return response()->json([
                    'id' => $mychange->id,
                    'success' => 'Los cambios se han guardado correctamente'
                ]);

            } else {
                throw new \Exception('Error al guardar los cambios');
            }

        } catch (\Throwable $e) {
            DB::rollBack();

            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Obtener listado de Mis Cambios registrados por el usuario autenticado.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        /** @var User $user Usuario autenticado */
        $user = auth()->user();

        // Consultamos los cambios en orden de mas recientes a mas antiguos
        $perPage = (int)$request->get('per_page');
        $result = $user->mychanges()->orderBy('updated_at', 'desc')->paginate($perPage);

        return response()->json($result);
    }

    /**
     * Obtener los datos de un registro de mis cambios asociado al id.
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function getAttributes($id)
    {
        /** @var User $user Usuario autenticado */
        $user = auth()->user();

        return $user->mychanges()->find($id);
    }

    /**
     * Obtener detalles del cambio indicado.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetails(int $id, Request $request)
    {
        // Validamos input
        $validationRules = [
            'per_page' => 'nullable|integer|min:1',
        ];
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        /** @var User $user Usuario autenticado */
        $user = auth()->user();

        if ($id > 0) {
            // Buscamos registro de Mis Cambios del usuario actual con el id indicado
            $mychange = $user->mychanges()->where('id', $id)->first();
        } else {
            // Buscamos el registro temporal del usuario
            $mychange = $user->mychanges()->where('status', 'temporal')->first();
        }

        if ($mychange) {
            /** @var LengthAwarePaginator $result */
            $perPage = (int)$request->input('per_page', 100);
            $result = $mychange->details()->paginate($perPage);

            return response()->json($result);
        } else {
            return response()->json(['error'=>'Registro no encontrado'], 404);
        }
    }

    /**
     * Elimina un registro de mis cambios y sus detalles.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        /** @var User $user Usuario autenticado */
        $user = auth()->user();

        // Validamos input
        $validationRules = [
            'id' => ['required','integer','min:0'],
        ];
        if ($request->id != 0){
            $validationRules['id'][] = Rule::exists((new MyChange())->getTable())
                ->where(function ($query) use ($user) {
                    return $query->where('user_id', $user->id);
                });
        }
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if ($request->id == 0) {
            $mychange = $user->mychanges()->where('status', 'temporal');
        } else {
            $mychange = $user->mychanges()->find($request->id);
        }
        $mychange->delete();

        return response()->json(['success'=>'El cambio fue eliminado exitosamente']);
    }

    /**
     * Exporta a un archivo excel los detalles registrados en Mis Cambios con el id indicado.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export($id)
    {
        /** @var User $user Usuario autenticado */
        $user = auth()->user();

        /** @var MyChange $mychange Mis Cambios a exportar */
        $mychange = $user->mychanges()->find($id);

        if ($mychange) {
            return (new MyChangesExport($id))->download($mychange->name . '.xlsx');
        } else {
            return response()->json(['error'=>'Registro no encontrado'], 404);
        }
    }

    /**
     * Elimina un registro de los detalles de un conjunto de mis cambios.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteDetail(int $id, Request $request)
    {
        $changeDetail = MyChangeDetail::find($id);
        if (!$changeDetail) {
            return response()->json(['code' => 'change-detail/not-found'], 404);
        }

        $changeDetail->delete();

        return response()->json(["id" => $changeDetail->id]);
    }

    /**
     * Actualizar Valor Actual desde los datos Originales del registro de
     * Mis Cambios indicado por el id.
     *
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function updateFromOriginalData($id)
    {
        /** @var User $user Usuario autenticado */
        $user = auth()->user();

        /** @var MyChange $mychange Mis Cambios a actualizar */
        $mychange = $user->mychanges()->find($id);

        if ($mychange) {
            $affected = $this->updateCurrentValues($mychange);

            return ['success'=>'Se han actualizado los registros exitosamente', 'affected' => $affected];
        } else {
            return response()->json(['error'=>'Registro no encontrado'], 404);
        }
    }

    /**
     * Actualiza el campo Valor Actual del detalle de los cambios
     * con los de la tabla original a la que pertenecen los datos.
     *
     * @param $mychange MyChange
     * @return int
     * @throws \Throwable
     */
    private function updateCurrentValues($mychange)
    {
        $details = $mychange->details()->select(['tech','mo','param'])->distinct()->get();
        $affected = 0;
        DB::beginTransaction();

        try {
            foreach ($details as $detail) {
                // Buscamos los registros del indice
                $indexList = DbMoIndex::where([
                    'tech' => $detail->tech,
                    'mo' => $detail->mo,
                ])->get();

                // Genero las columnas de Elementos de Red
                $elemsRed = [];
                foreach ($indexList as $index) {
                    $alias = strtolower($index->ELEMENTO_DE_RED);

                    if ($alias == 'celda_tdd' || $alias == 'celda_fdd') {
                        $alias = self::ELEM_RED_CELDA;
                        $elemsRed[$alias][] = $index->COLUMN_NAME_DB;
                    } else {
                        $elemsRed[$alias] = $index->COLUMN_NAME_DB;
                    }
                }

                // Si existe 'celda' como elemento de red reemplazamos el marcador __CELDA__
                // por el COALESCE de cada columna
                if (isset($elemsRed[self::ELEM_RED_CELDA]) && count($elemsRed[self::ELEM_RED_CELDA]) > 0) {
                    $columns = implode(',', $elemsRed[self::ELEM_RED_CELDA]);
                    $elemsRed[self::ELEM_RED_CELDA] = "COALESCE($columns)";
                }

                // Armamos join de elem red
                $join = "mcd.date = DATE(varDateTime)";
                foreach ($elemsRed as $name => $column) {
                    $join .= " AND mcd.$name = $column";
                }

                $table = $indexList[0]->TABLE_NAME_DB;

                // Obtengo nombre de la base de datos
                $edr_db = config('database.connections.mysql.database');

                $sql = "UPDATE $edr_db.my_changes_details mcd
                            INNER JOIN $table ON $join
                          SET mcd.current_value = {$detail->param}
                        WHERE mcd.my_changes_id = ?
                          AND mcd.tech = ?
                          AND mcd.mo = ?
                          AND mcd.param = ?";

                $affected += DB::connection('export_dr')
                    ->update($sql, [
                        $mychange->id,
                        $detail->tech,
                        $detail->mo,
                        $detail->param,
                    ]);
            }

            DB::commit();

            return $affected;

        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function saveMany(Request $request)
    {
        // Validamos input
        $validationRules = [
            'new_value' => 'required'
        ];

        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $dashboardController = new DashboardController();

        // Validamos input y obtenemos la instancia del query
        $qry = $dashboardController->buildQuery($request, false);

        if ($qry instanceof JsonResponse)
            return $qry;

        // Validamos si existe datos con los filtros dados
        $totalRows = $qry->count();
        if ($totalRows == 0)
            return response()->json(['error'=>'No se encontraron datos con los parametros indicados'], 400);

        // Conexión a usar
        $db = DB::connection('export_dr');
        $db->beginTransaction();

        try {
            // Extract input
            $tech = $request->tech;
            $mo = $request->mo;
            $param = $request->param;
            $new_value = $request->new_value;
            $created_at = date('Y-m-d H:i:s');
            $deletedRows = 0;

            $setOrLdn = $dashboardController->setOrLdn;

            // Obtengo elementos de red
            $elemsRed = $dashboardController->elemsRed[0];

            // Obtengo nombre de la base de datos
            $edr_db = config('database.connections.mysql.database');

            // Obtengo el registro del temporal
            $mychange = $this->getOrCreateTemporal();
            if ($mychange->exists) {
                // Existe un temporal hay que limpiar los registros existentes que se puedan duplicar
                $table = (new MyChangeDetail())->getTable();

                $qry2 = clone $qry;

                $qry2->join("$edr_db.$table AS mc", function ($join) use ($mychange, $setOrLdn, $tech, $mo, $param, $elemsRed) {
                    $join->on(DB::raw('DATE(varDateTime)'), '=', 'mc.date')
                        ->on("t.$setOrLdn", '=', "mc.$setOrLdn")
                        ->where('mc.tech', '=', $tech)
                        ->where('mc.mo', '=', $mo)
                        ->where('mc.param', '=', $param)
                        ->where('mc.my_changes_id', '=', $mychange->id);

                    foreach ($elemsRed as $name => $column) {
                        $join->on(DB::raw($column), '=', "mc.$name");
                    }
                });

                $deleteSql = $qry2->grammar->compileDelete($qry2);

                $replaced = 0;
                $deleteSql = str_ireplace('delete `t`', 'delete `mc`', $deleteSql, $replaced);

                if ($replaced === 1) {
                    $deletedRows = $db->delete($deleteSql, $qry2->getBindings());
                } else {
                    throw new \Exception('Error replacing delete statement');
                }
            } else {
                $mychange->save();
            }

            // Armamos las columnas con sus respectivos valores
            $columns = array_merge(['date', $setOrLdn], array_keys($elemsRed), ['current_value',
                'tech',
                'mo',
                'param',
                'new_value',
                'my_changes_id',
                'created_at',
                'updated_at',
            ]);
            $columns = array_map(function ($col) {
                return "`$col`";
            }, $columns);

            $insertColumns = implode(',', $columns);

            // Agregamos columnas faltantes en el select del query
            $qry->selectRaw("'$tech','$mo','$param','$new_value',{$mychange->id},'$created_at','$created_at'");

            // Obtengo los datos de la tabla de mis cambios
            $table = (new MyChangeDetail())->getTable();

            // Creamos el insert
            $insertQry = "INSERT INTO $edr_db.$table ($insertColumns) " . $qry->toSql();

            // Ejecutamos el insert
            $inserted = $db->insert($insertQry, $qry->getBindings());

            if ($inserted) {
                $db->commit();

                return [
                    'success' => 'Cambios guardados correctamente en el temporal',
                    'rows' => $totalRows,
                    'deletedRows' => $deletedRows,
                ];
            } else {
                throw new \Exception('Something happened on insert');
            }

        } catch (\Exception $e) {
            $db->rollBack();

            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
