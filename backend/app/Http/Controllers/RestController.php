<?php

namespace App\Http\Controllers;

use App;
use Route;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

/**
 * Clase estandar para asociar una rutas a un resource estilo CRUD.
 * Se recomienda utilizar los métodos:
 * - createItem
 * - readItem
 * - updateItem
 * - deleteItem
 * - readList
 */
class RestController extends CrudController
{
    /**
     * Modelos asociados a una ruta rest.
     */
    protected $models = [
        ['route' => 'api/plannedareas', 'model' => App\PlannedArea::class],
    ];

    /**
     * Mostrar una lista de elementos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->readList();
    }

    /**
     * Muestra el formulario de creación.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Guarda un nuevo elemento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate($request);

        return $this->createItem($data);
    }

    /**
     * Muestra un elemento.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->readItem($id);
    }

    /**
     * Muestra el formulario de edición.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Actualiza un elemento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->validate($request);

        return $this->updateItem($data, $id);
    }

    /**
     * Remueve un elemento.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->deleteItem($id);
    }
}
