<?php

namespace App\Http\Controllers;

use App\DbPlanAreaIndex;
use App\DbBaselineIndex;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ExportDrController extends Controller
{
    /**
     * Obtiene la lista de Tecnologías.
     * @param Request $request
     * @return Collection|array
     */
    public function getTechList(Request $request)
    {
        $list = DbPlanAreaIndex::tech();

        if ($request->with === 'type') {
            $list->select(['tech', 'elemento_de_red as net_type']);

            if ($tech = $request->tech) {
                $list->where('tech', $tech);
            }

            return $list->get()->each(function($el){
                $el->net_type = strtolower($el->net_type);
            });
        }

        return $list->get()->pluck('tech');
    }

    /**
     * Obtiene la lista de Baselines.
     * @param Request $request
     * @return Collection|array
     */
    public function getBaselineList(Request $request)
    {
        $list = DbBaselineIndex::tech();
       
       return $list->get();
    }

    /**
     * Obtiene la lista de Templates.
     * @param Request $request
     * @return Collection|array
     */
    public function getTemplateList(){
        $list = DbBaselineIndex::techTemplate();       
       return $list->get();
    }

    /**
     * Obtiene el listado nacional de una tecnología seleccionada.
     *
     * @param \Illuminate\Http\Request $request
     * @param $type
     * @return \Illuminate\Http\Response
     */
    public function getNationalElements(Request $request, $tech = 'LTE')
    {
        $tech = strtoupper($tech);
        $class = "\\App\\Models\\Dr\\ListadoNacional{$tech}";

        $data = $request->validate([
            'net_type' => 'required|alpha_dash',
        ]);

        $elements = $class::distinct()
            ->select($data['net_type'])
            ->inRandomOrder()
            ->limit(100000)
            ->get()
            ->pluck($data['net_type'])
            ->toArray();

        asort($elements);

        return array_values($elements);
    }

    /**
     * Obtiene los elementos disponibles para area planeada del listado nacional.
     *
     * @param \Illuminate\Http\Request $request
     * @param $type
     * @return \Illuminate\Http\Response
     */
    public static function getNationalCells(Request $request, $tech)
    {
        $rows = PlannedAreaController::$rows;
        $data = $request->validate([
            'net_type' => 'required|alpha_dash',
            'net_elements' => 'required',
        ]);
        $tech = strtoupper($tech);
        $type = strtolower($data['net_type']);
        if (starts_with($type, 'celda')) $type = 'celda';
        $elements = array_wrap($data['net_elements']);

        $class = "\\App\\Models\\Dr\\ListadoNacional{$tech}";

        return $class::select($rows[$tech])
            ->distinct()
            ->whereIn($type, $elements)
            ->get();
    }
}
