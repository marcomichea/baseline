<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\User;


class UserController extends Controller
{
    public static $customAttributes = [
        'name' => 'Nombre',
        'last_name' => 'Apellido',
        'rut' => 'RUT',
        'is_admin' => 'Perfil',
        'phone_number' => 'Teléfono Fijo',
        'mobile_number' => 'Teléfono Móvil',
        'email' => 'Correo Electrónico',
        'password' => 'Contraseña',
        'new_password' => 'Confirmar Contraseña',
    ];

    public function getAll(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'page' => 'required|numeric',
            'per_page' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        if (! auth()->user()->is_admin) {
            return response()->json(['code' => 'auth/admin-level-required'], 403);
        }

        $perPage = (int)$request->get('per_page');
        $users = DB::table('users')->paginate($perPage);

        return response()->json($users);
    }

    public function get(int $id)
    {
        if (! auth()->user()->is_admin) {
            return Response::json(['error' => 'auth/admin-level-required'], 403);
        }

        $user = User::find($id);
        if (!$user) {
            return response()->json(['code' => 'user/not-found'], 404);
        }

        return response()->json($user);
    }

    public function create(Request $request)
    {
        if (! auth()->user()->is_admin) {
            return Response::json(['code' => 'auth/admin-level-required'], 403);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|alpha_spaces|max:255',
            'last_name' => 'required|alpha_spaces|max:255',
            'rut' => 'required|alpha_dash|min:4',
            'is_admin' => 'required|boolean',
            'phone_number' => 'nullable|numeric',
            'mobile_number' => 'nullable|numeric',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6|max:20',
        ], [], static::$customAttributes);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'last_name' => $request->get('last_name'),
            'rut' => $request->get('rut'),
            'is_admin' => $request->get('is_admin'),
            'phone_number' => $request->get('phone_number'),
            'mobile_number' => $request->get('mobile_number'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        return Response::json($user);
    }

    public function update(int $id, Request $request)
    {
        if (! auth()->user()->is_admin) {
            return Response::json(['code' => 'auth/admin-level-required'], 403);
        }

        $user = User::find($id);
        if (!$user) {
            return response()->json(['code' => 'user/not-found'], 404);
        }

        $validationRules = [
            'name' => 'required|alpha_spaces|max:255',
            'last_name' => 'required|alpha_spaces|max:255',
            'rut' => 'required|alpha_dash|min:4',
            'is_admin' => 'required|boolean',
            'phone_number' => [
                'nullable',
                'regex:/(^([0-9-+]*)?$)/u',
            ],
            'mobile_number' => [
                'nullable',
                'regex:/(^([0-9-+]*)?$)/u',
            ],
            'email' => [
                'required', 'string', 'email', 'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
        ];
        $validator = Validator::make($request->all(), $validationRules, [], static::$customAttributes);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $user->fill($request->only(array_keys($validationRules)));
        $user->save();

        return response()->json($user);
    }

    public function delete(int $id)
    {
        if (! auth()->user()->is_admin) {
            return Response::json(['code' => 'auth/admin-level-required'], 403);
        }

        if ($id == 1) {
            return Response::json(['code' => 'auth/main-admin-is-undeletable'], 400);
        }

        $user = User::find($id);
        if (!$user) {
            return response()->json(['code' => 'user/not-found'], 404);
        }

        $user->delete();

        return response()->json(["id" => $user->id]);
    }
}
