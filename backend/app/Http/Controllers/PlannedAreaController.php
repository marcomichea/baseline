<?php

namespace App\Http\Controllers;

use Auth;
use App\PlannedArea;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PlannedAreaController extends RestController
{
    /**
     * Columnas requeridas de cada tecnología.
     */
    public static $rows = [
        '2G' => ['bsc', 'cluster', 'sitio', 'celda', 'responsable_cluster'],
        '3G' => ['rnc', 'cluster', 'sitio', 'celda', 'responsable_cluster'],
        'LTE' => ['region', 'cluster', 'sitio', 'celda', 'responsable_cluster'],
    ];

    protected $validators = [
        'name' => ['required','max:100'],
        'tech' => 'required|max:50|alpha_unspaced',
        'net_type' => 'required|max:100|alpha_unspaced',
        'net_elements' => 'required|array',
        'net_elements.*' => 'required|string',
    ];

    /** @var  string Escenario para el cual se aplican las reglas de validación */
    protected $scenario = 'create';

    protected $customAttributes = [
        'name' => 'Nombre',
        'tech' => 'Tecnología',
        'net_type' => 'Tipo de Elemento de Red',
        'net_elements' => 'Elementos de Red',
    ];

    protected $messages = [
        'net_elements.*.required' => 'No se permiten elementos vacíos en el listado seleccionado'
    ];

    /**
     * Devuelve el modelo asociado a la ruta actual.
     *
     * @return string
     */
    protected function model() {
        return PlannedArea::class;
    }

    /**
     * Crea una instancia del modelo.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function instance()
    {
        return PlannedArea::where('user_id', Auth::user()->id);
    }

    /**
     * Guarda un nuevo elemento con sus celdas.
     *
     * @param  \Illuminate\Http\Request $request
     * @return PlannedArea|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $values = $this->getCells($request);

        if ($values instanceof JsonResponse)
            return $values;

        $plannedArea = new PlannedArea($values['data']);
        $plannedArea->user_id = Auth::user()->id;
        $plannedArea->save();

        $plannedArea->cells()->createMany($values['cells']->toArray());

        return $plannedArea;
    }

    /**
     * Actualiza un registro y sus detalles.
     *
     * @param Request $request
     * @param $id
     * @return array|JsonResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->scenario = 'update';

        $values = $this->getCells($request);

        if ($values instanceof JsonResponse)
            return $values;

        $plannedArea = $this->updateItem($values['data'], $id);

        $plannedArea->cells()->delete();

        $plannedArea->cells()->createMany($values['cells']->toArray());

        return $plannedArea;
    }

    /**
     * Obtiene y valida los detalles de la data de los elementos seleccionados.
     * Filtra listado de elementos seleccionados quitando los que no existen en la data.
     *
     * @param Request $request
     * @param $plannedArea
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getCells(Request $request)
    {
        $data = $this->validate($request);
        extract($data);

        $rows = static::$rows;
        $listadoNacional = "\\App\\Models\\Dr\\ListadoNacional{$tech}";

        /** @var Collection $cells */
        $cells = $listadoNacional::distinct()
            ->select($rows[$tech])
            ->whereIn($net_type, $net_elements)
            ->get();

        if ($cells->isNotEmpty()) {
            // Filtramos los elementos seleccionados con los existentes en los datos
            $data['net_elements'] = array_values(array_intersect($net_elements, $cells->pluck($net_type)->toArray()));

            return compact('data','cells');

        } else {
            return response()->json([
                'errors'=>[
                    'msg' => ['No existen registros asociados a los elementos seleccionados']
                ]
            ], 422);
        }
    }

    public function validate(Request $request = null, array $rules = null, array $messages = [], array $customAttributes = [])
    {
        if ($this->scenario == 'create' || $this->scenario == 'update') {

            $unique = Rule::unique((new PlannedArea())->getTable())->where(function ($query) {
                return $query->where('user_id', auth()->user()->id);
            });

            if ($this->scenario == 'update')
                $unique->ignore($request->route('plannedarea'));

            $this->validators['name'][] = $unique;
        }
        return parent::validate($request, $rules, $messages, $customAttributes);
    }
}
