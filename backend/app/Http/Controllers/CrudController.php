<?php

namespace App\Http\Controllers;

use Route;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

/**
 * Crud Controller.
 * Permite realizar las acciones básicas de un Crud estandar
 */
class CrudController extends Controller
{
    /**
     * Modelos asociados a una ruta rest.
     */
    protected $models = [];

    /**
     * Validadores.
     */
    protected $validators = [];

    /**
     * @var array Nombres de atributos personalizados para los mensajes de validación
     */
    protected $customAttributes = [];

    /**
     * @var array Mensajes de validación personalizados
     */
    protected $messages = [];

    /**
     * Devuelve el modelo asociado a la ruta actual.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function model()
    {
        $model = collect($this->models)->first(function($model){
            return starts_with(Route::current()->uri, $model['route'] ?? null);
        })['model'] ?? null;

        // Si no existe
        if (!$model) abort(404);

        return $model;
    }

    /**
     * Valida un request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Array
     */
    public function validate(Request $request = null, array $rules = null, array $messages = [], array $customAttributes = [])
    {
        $request = $request ?: request();
        $rules = $rules ?? $this->validators;
        $customAttributes = $customAttributes ?: $this->customAttributes;
        $messages = $messages ?: $this->messages;

        return empty($rules) ? $request->all() : parent::validate($request, $rules, $messages, $customAttributes);
    }

    /**
     * Crea una instancia del modelo.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function instance()
    {
        // Si no existe el modelo devolvemos 404
        if (!($model = $this->model())) abort(404);

        return new $model;
    }

    /**
     * Busca un elemento.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function find($id)
    {
        $instance = $this->instance();
        $element = $id instanceof $instance ? $id : $instance->findOrFail($id);
        if ($element && !$element->exists) {
            abort(204, 'No se pudo encontrar el elemento.');
        }

        return $element;
    }

    /**
     * Mostrar una lista de elementos.
     *
     * @param  callable  $callback
     * @return \Illuminate\Http\Response
     */
    public function readList(callable $callback = null)
    {
        $instance = $this->instance();
        if ($callback) $callback($instance);

        return $instance->get();
    }

    /**
     * Guarda un nuevo elemento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createItem(array $data)
    {
        $model = $this->model();
        $insert = new $model($data);

        if (!$insert->save()) {
            abort(204, 'No se pudo crear el elemento.');
        }

        return $insert;
    }

    /**
     * Muestra un elemento.
     *
     * @param  \Illuminate\Database\Eloquent\Model or String  $id
     * @return \Illuminate\Http\Response
     */
    public function readItem($id)
    {
        $element = $this->find($id);

        return response()->json($element);
    }

    /**
     * Actualiza un elemento.
     *
     * @param  Array  $data
     * @param  \Illuminate\Database\Eloquent\Model or String  $id
     * @return \Illuminate\Http\Response
     */
    public function updateItem(array $data, $id)
    {
        $element = $this->find($id);

        if (!$element->fill($data)->save()) {
            abort(204, 'No se pudo actualizar el elemento.');
        }

        return $element;
    }

    /**
     * Remueve un elemento.
     *
     * @param  \Illuminate\Database\Eloquent\Model or String  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteItem($id)
    {
        $item = $this->find($id);
        $data = [
            'value' => $item ? $item->toArray() : null,
        ];
        $data['deleted'] = $item->delete();
        return $data;
    }
}
