<?php

namespace App\Http\Controllers;

use App\Download;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class DownloadsController extends Controller
{
    /**
     * Obtiene registro de la descarga asociada a un id.
     *
     * @param $id
     * @return Download|\Illuminate\Http\JsonResponse
     */
    public function getDownloadById($id)
    {
        $download = Download::find($id);

        if ($download) {
            return $download;
        } else {
            return response()->json(['error' => 'El id de registro indicado no es válido'], 400);
        }
    }

    /**
     * Obtiene listado de descargas en estatus 'pending' y 'ready' para el usuario logueado.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse
     */
    public function getDownloadsByUser(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        if ($user) {
            $status = ['pending', 'ready'];

            if ($request->status == '0'){
                $status = ['pending'];
            }
            if ($request->status == '1'){
                $status = ['ready'];
            }

            return $user->downloads()->whereIn('status', $status)
                ->paginate($request->input('perPage', 10))->appends($request->except('page'));
        } else {
            return response()->json(['error' => 'Debe iniciar sesión con un usuario válido'], 400);
        }
    }

    /**
     * Envía la descarga del archivo asociada a un id.
     *
     * @param $id
     * @return BinaryFileResponse | \Illuminate\Http\JsonResponse
     */
    public function getFile($id)
    {
        $download = Download::find($id);

        if (!$download)
            return response()->json(['error' => 'El id de descarga indicado no es válido'], 400);

        switch ($download->status){
            case 'ready':
                $file = storage_path('app') . DIRECTORY_SEPARATOR . $download->filename;
                if (file_exists($file)) {
                    $download->status = 'downloaded';

                    if ($download->save())
                        return response()->download($file)->deleteFileAfterSend();
                    else
                        return response()->json(['error' => 'No se pudo procesar la descarga'], 500);
                }
                else
                    return response()->json(['error' => 'No se pudo procesar la descarga. No se encuentra el archivo'], 500);
                break;

            case 'pending':
                $msg = 'El archivo no está listo para su descarga';
                break;

            case 'downloaded':
                $msg = 'Esta descarga ya fue procesada';
                break;
        }
        return response()->json(['error' => $msg], 400);
    }
}
