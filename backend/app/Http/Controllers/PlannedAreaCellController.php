<?php

namespace App\Http\Controllers;

use App\PlannedArea;
use Illuminate\Http\Request;

class PlannedAreaCellController extends RestController
{
    /**
     * Crea una instancia del modelo.
     *
     * @param $planned_area_id
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function instance($planned_area_id = null)
    {
        $plannedArea = PlannedArea::find($planned_area_id);
        return $plannedArea ? $plannedArea->cells() : null;
    }


    /**
     * Mostrar una lista de elementos.
     *
     * @param $planned_area_id
     * @return \Illuminate\Http\Response
     */
    public function index($planned_area_id = null)
    {
        return $this->instance($planned_area_id)->get();
    }
}
