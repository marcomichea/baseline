<?php

namespace App\Http\Controllers;

use App\Notifications\RecoverPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;

class APILoginController extends Controller
{
    /**
     * Realiza el login del usuario y retorna el token.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password'=> 'required'
        ], [], UserController::$customAttributes);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Credenciales Incorrectas'], 400);
            }
            $valid_time = config('jwt.ttl');

        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token', 'valid_time'));
    }

    /**
     * Realiza el cambio de contraseña para el usuario actual
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request)
    {
        // Obtengo el usuario logueado
        $user = auth()->user();

        if ($user) {
            // Validamos input
            $validationRules = [
                'password' => ['required', function ($attribute, $value, $fail) use ($user) {
                    if (!\Hash::check($value, $user->password)) {
                        return $fail(__('La contraseña actual es incorrecta'));
                    }
                    return true;
                }],
                'new_password' => 'required|string|max:255|confirmed',
            ];
            $validator = Validator::make($request->all(), $validationRules, [], UserController::$customAttributes);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 400);
            }

            // Asignamos el nuevo password
            $user->password = bcrypt($request->get('new_password'));
            $user->save();

            return response()->json(['success'=>'Su contraseña se ha cambiado correctamente']);

        } else {
            return response()->json(['error'=>'Sesión inválida. Debe iniciar sesión para cambiar su contraseña.'], 401);
        }
    }

    /**
     * Actualizar datos del perfil del usuario
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserProfile(Request $request)
    {
        // Obtengo el usuario logueado
        $user = auth()->user();

        if ($user) {
            // Validamos input
            $validationRules = [
                'email' => [
                    'required', 'string', 'email', 'max:255',
                    Rule::unique('users')->ignore($user->id),
                ],
                'name' => 'required|alpha_spaces|max:255',
                'last_name' => 'required|alpha_spaces|max:255',
                'rut' => 'required|alpha_dash',
                'is_admin' => 'required|boolean',
                'phone_number' => 'nullable|numeric',
                'mobile_number' => 'nullable|numeric',
            ];
            $validator = Validator::make($request->all(), $validationRules, [], UserController::$customAttributes);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 400);
            }

            // Actualizamos el registro
            $user->fill($request->only(array_keys($validationRules)));
            $user->save();

            return response()->json(['success'=>'Su perfil se ha actualizado correctamente']);

        } else {
            return response()->json(['error'=>'Sesión inválida. Debe iniciar sesión para actualizar su perfil.'], 401);
        }
    }

    /**
     * Recuperar contraseña olvidada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendRecoverPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|exists:users,email',
        ], [], UserController::$customAttributes);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        /** @var User $user */
        $user = User::where('email', $request->email)->first();

        if ($user) {
            // Genero random password
            $pwd = str_random(8);

            // Guardo la nueva clave
            $user->password = bcrypt($pwd);

            if ($user->save()) {
                // Enviamos correo
                $user->notify(new RecoverPassword($user, $pwd));

                return response()->json([
                    'success'=>'Correo de recuperación de contraseña enviado correctamente',
                    'url' => config('app.urls.login')
                ]);
            } else {
                return response()->json(['error'=>'Fallo al salvar usuario'], 500);
            }
        } else {
            return response()->json(['error'=>'Usuario no encontrado']);
        }
    }

    /**
     * Refresca el token vencido por uno nuevo.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken(Request $request)
    {
        $token = JWTAuth::getToken();
        if(!$token){
            return response()->json(['error'=>'token_not_provided'], 401);
        }
        try{
            $token = JWTAuth::refresh($token);
            $valid_time = config('jwt.ttl');
        }catch(TokenInvalidException $e){
            return response()->json(['error'=>'token_invalid'], 401);
        }catch(TokenExpiredException $e){
            return response()->json(['error'=>$e->getMessage()], 401);
        }
        return response()->json(compact('token', 'valid_time'));
    }

}
