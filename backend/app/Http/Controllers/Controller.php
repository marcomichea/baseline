<?php

namespace App\Http\Controllers;

use App\DbBaselineMOIndex;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Obtiene listado de tecnologías del índice
     *
     * @return array
     */
    public function getIndexTechList()
    {
        return DbBaselineMOIndex::tech()->get()->pluck('technology');
    }
}
