<?php

namespace App\Http\Controllers;

use App\DbHwColumns;
use App\PlannedArea;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class HardwareController extends DashboardController
{
    /** @var  array Lista de columnas fijas a mostrar para MO seleccionado */
    protected $fixedColumns;

    /**
     * Obtiene la lista de campos de la tabla asociada al MO seleccionado.
     * Con las exclusiones correspondientes.
     *
     * @param Request $request
     * @return array|JsonResponse
     */
    public function getParamsList(Request $request)
    {
        // Validamos input
        $validationRules = [
            'tech' => ['required',Rule::in($this->getTechList($request))],
            'mo' => 'required|string',
        ];
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        // Campos a excluir
        $excludeColumns = config('app.excludeColumns');

        // Obtengo registros seleccionados del indice
        /** @var Collection $indexList */
        $indexList = $this->getIndex()::where($request->only(array_keys($validationRules)))->get();

        if ($indexList->isNotEmpty()) {
            $table = $indexList[0]->TABLE_NAME_DB;

            // Defino la conexión a usar
            $schema = Schema::connection('export_dr');

            // Obtengo listado de columnas de la tabla asociada al mo seleccionado
            $paramsList = $schema->getColumnListing($table);

            foreach ($indexList as $index) {
                // Actualizamos campos a excluir
                $excludeColumns[] = $index->COLUMN_NAME_DB;
            }

            // Aplicamos filtrado de campos a excluir
            $paramsList = array_values(array_diff($paramsList, $excludeColumns));

            // Ordenamos elementos alfabeticamente
            sort($paramsList, SORT_NATURAL | SORT_FLAG_CASE);

            return $paramsList;

        } else {
            return response()->json(['error'=>'No se encontraron registros en el indice con los parámetros indicados'], 400);
        }
    }

    /**
     * Construye la instancia del query builder para su ejecución y reuso en distintos endpoints.
     *
     * @param Request $request
     * @param bool $withTemporal Indica si se debe consultar los cambios temporales
     * @param User $user
     * @return Builder|JsonResponse
     */
    public function buildQuery(Request $request, $withTemporal = false, $user = null)
    {
        /** @var User $user Usuario autenticado */
        $user = $user ? $user : auth()->user();

        // Build rules for filter params
        $columnsFilterRules = array_fill_keys($this->columnsFilter, 'nullable|string');

        // Validamos input
        $validationRules = [
                'planned_area_id' => ['nullable','integer','min:1',
                    Rule::exists((new PlannedArea())->getTable(), 'id')->where(function ($query) use ($user) {
                        return $query->where('user_id', $user->id);
                    })],
                'date' => 'nullable|date',
                'tech' => ['required',Rule::in($this->getTechList($request))],
                'mo' => 'required|string',
                'param' => 'required|string',
                'rowsPerPage' => 'nullable|integer|min:1',
            ] + $columnsFilterRules;

        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        // Obtenemos los elementos de red
        list($elemsRed, $columnsElemRed) = $this->getElemsRedByTechMO(
            $request->input('tech'),
            $request->input('mo')
        );

        // Buscamos los registros del indice
        $indexList = $this->indexList;

        if (count($indexList) > 0) {
            // Definimos los valores para el query
            $tech = $indexList[0]->TECH;
            $mo = $indexList[0]->MO;
            $table = $indexList[0]->TABLE_NAME_DB;
            $param = $request->input('param');

            // Obtengo columnas fijas
            $fixedColumns = $this->getFixedColumns($mo, $elemsRed);

            if (count($fixedColumns) > 0) {
                // Formateamos las columnas para el select
                array_walk($fixedColumns, function (&$column) {
                    $column = "t.`$column`";
                });
                $fixedColumns = implode(', ', $fixedColumns) . ',';
            } else {
                $fixedColumns = '';
            }

            // Armamos el query
            $qry = DB::connection('export_dr')
                ->table($table.' as t')
                ->selectRaw("DATE(varDateTime) AS date, $fixedColumns $columnsElemRed `$param` AS current_value")
                ->whereRaw($this->buildWhere($request, $elemsRed, $param));

            return $qry;

        } else {
            return response()->json(['error'=>'No se encontraron datos en el indice'], 400);
        }
    }

    /**
     * Obtiene listado de columnas fijas para el MO seleccionado
     * excluyendo las que ya son elementos de red
     *
     * @param $mo string MO
     * @param $elemsRed array Elementos de Red
     * @return array
     */
    public function getFixedColumns($mo, $elemsRed)
    {
        if (!isset($this->fixedColumns)) {
            $this->fixedColumns = DbHwColumns::where('mo', $mo)->get()->pluck('Columnas')->toArray();

            // Excluimos las que ya son elementos de red
            $elmRedColumns = array_map(function ($column) {
                return trim($column, '`');
            }, $elemsRed);

            $this->fixedColumns = array_diff($this->fixedColumns, $elmRedColumns);
        }

        return $this->fixedColumns;
    }

    /**
     * Obtiene los nombres de las columnas para la tabla de datos principal.
     *
     * @param Request $request
     * @return array|JsonResponse
     */
    public function getDataHeaders(Request $request)
    {
        $columns = parent::getDataHeaders($request);

        if ($columns instanceof JsonResponse)
            return $columns;

        $fixedColumns = $this->getFixedColumns($request->mo, $this->getElemsRedByTechMO($request->tech, $request->mo)[0]);

        array_splice($columns, 1, 0, $fixedColumns);

        return $columns;
    }
}
