<?php

namespace App\Http\Controllers;

use App\DbHwIndex;
use App\DbMoIndex;
use App\DbBaselineIndex;
use App\DbBaselineMOIndex;
use App\Download;
use App\Exports\DataExport;
use App\Jobs\ExportDataToCsv;
use App\LastUpdatedDate;
use App\MyChange;
use App\PlannedArea;
use App\PlannedAreaCell;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Csv\Writer;

class DashboardController extends Controller
{
    /** @var array Lista de filtros por columnas permitidos al dataset */
    protected $columnsFilter = ['filter_mo', 'filter_bsc', 'filter_rnc', 'filter_region', 'filter_cluster',
        'filter_sitio', 'filter_celda', 'filter_param', 'filter_new_value', 'filter_ldn', 'filter_set'];

    /** Nombre del Elemento de Red 'celda' como se debe visualizar en el dataset */
    const ELEM_RED_CELDA = 'celda';

    /** @var  boolean Indica si el usuario actual posee cambios temporales en Mis Cambios */
    protected $hasTemporalChanges = false;

    /** @var  Collection|array Registros del indice para la tecnología y MO seleccionados */
    public $indexList;

    /** @var  string Nombre de la base de datos principal edr_users */
    public $edr_db;

    /**
     * index[0] = Arreglo que contiene los elementos de red para la tecnología y MO seleccionados con la forma:
     *      ['sitio' => 'columna',...]
     * index[1] = Cadena con las columnas que representan los elementos de red para usar como select en un query.
     * @var  array
     */
    public $elemsRed;

    /** @var  string Contiene el nombre de la columna correspondiente a la tecnología (set|ldn) */
    public $setOrLdn;

    /** @var  string Última fecha de los datos */
    public $date;

    /**
     * Indica si se ejecuta el controlador Hardware
     *
     * @return bool
     */
    public function isHardware()
    {
        return $this instanceof HardwareController;
    }

    /**
     * Retorna el modelo del indice correspondiente.
     *
     * @return string
     */
    public function getIndex()
    {
        if ($this->isHardware())
            return DbHwIndex::class;
        else
            return DbBaselineMoIndex::class;
    }

    /**
     * Guarda los datos por Default del template elegido
     * en un nuevo Baseline.
     * 
     * @param Request $request
     * @return array|JsonResponse
     */
    public function saveNewBaseline(Request $request){

        $fechaActual = date("Y-m-d H:i:s");
        //inserta cabeceras en tabla Baseline y se genera id
        $qry = DB::connection('export_dr')
        ->table('Baseline')        
        ->insertGetId([
            'is_template'=>'0',
            'baseline_name'=> $request->baseline_name,
            'vendor'=>$request->vendor,
            'technology'=> $request->technology,
            'release'=> $request->release,
            'description'=> $request->description,
            'creation_date'=> $fechaActual,
            'user_id'=> $request->user_id,
            'user_name'=> $request->user_name,
            'fromTemplate'=> $request->fromTemplate
        ]);

        $idBaseline = $qry;

        $moTableList =$this->getIndex()::where('id_baseline','=',$request->fromTemplate)->get();
    
        $qry2 = null;
        $qry3 = null;
        //Inserta una linea en cada tabla de MO con los parametros por default
        foreach($moTableList as $moTable){
            $table = $moTable->mo_name;
            //Extrae la data por default a insertar
            $tableData = DB::connection('export_dr')
                        ->table($table)
                        ->where('id','=',$request->fromTemplate)
                        ->get();
            $tableDataMo = (array)$tableData[0];      
            //Se prepara data cambiando el ID para insertar      
            array_set($tableDataMo, 'id', $idBaseline);           
           
            //Inserta datos en las tablas de las MO
            $qry2 = DB::connection('export_dr')
            ->table($table)        
            ->insert($tableDataMo);

            //Guarda datos para tabla BaselineMo
            $tableDataBaselineMo= ['id_baseline'=> $idBaseline,'mo_name'=>$table,
                                    'mo' =>$moTable->mo, 'technology'=> $moTable->technology];
            
            //Inserta datos en BaselineMo
            $qry3 = DB::connection('export_dr')
            ->table('BaselineMo')        
            ->insert($tableDataBaselineMo);
        }
        
        if($qry && $qry2 && $qry3){
            return ['success'=> 'Nuevo baseline guardado exitosamente','data'=>$qry2];
        }else{
            return response()->json(['error'=>'error al insertar nuevo baseline' ,500]);
        }       
    } 

    /**
     * Realiza el update del parametro editado por MO
     *
     * @param Request $request
     * @return array|JsonResponse
     */
    public function saveChangesParamMo(Request $request){

        $param = $request->param;
        $id_baseline = $request->id_baseline;
        $newValue = $request->newValue;
        $oldValue = $request->oldValue;
        $comment = $request->comment;
        $userId = $request->userId;
        $userName = $request->userName;

        $validationRules = [
            'technology' => 'required|string',
            'mo' => 'required|string',
            'id_baseline' => 'required|string',
        ];
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        
        $dataList = $this->getIndex()::where($request->only(array_keys($validationRules)))->get();
         
        $table = $dataList[0]->mo_name;
        //Actualiza el valor por parametro
        $qry = DB::connection('export_dr')
        ->table($table.' as t')
        ->where('t.id',$id_baseline)
        ->update(array($param => $newValue));

        $fechaActual = date("Y-m-d H:i:s");
        //updatea fecha ultimo cambio en Baseline
        $qry2 = DB::connection('export_dr')
        ->table('Baseline')
        ->where('id_baseline',$id_baseline)
        ->update(array('last_update' => $fechaActual));

        $qry3 = DB::connection('export_dr')
        ->table('BaselineMoParam')
        ->updateOrInsert(
            array('id_baseline'=>$id_baseline, 'mo_name'=> $table, 'param_name'=>$param),
            array('id_baseline'=>$id_baseline, 'mo_name'=> $table, 'param_name'=>$param,
                'old_value'=>$oldValue, 'new_value'=>$newValue,'user_id'=>$userId ,'user_comment'=>$comment,
                'last_update'=> $fechaActual, 'user_name'=> $userName)
        );
        
        return ['success'=>'Se ha actualizado el registro exitosamente', 'affected' => $qry];
    }

    /**
     * Realiza el update del parametro editado por MO
     *
     * @param Request $request
     * @return array|JsonResponse
     */
    public function saveChangesBaselines(Request $request){

        $description = $request->description;
        $id_baseline = $request->id_baseline;
        $baseline_name = $request->baseline_name;

        $validationRules = [
            'description' => 'required|string',
            'baseline_name' => 'required|string',
            'id_baseline' => 'required|string',
        ];
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        

        $fechaActual = date("Y-m-d H:i:s");
        //updatea fecha ultimo cambio en Baseline
        $qry = DB::connection('export_dr')
        ->table('Baseline')
        ->where('id_baseline',$id_baseline)
        ->update(array('last_update' => $fechaActual,
                        'baseline_name' => $baseline_name,
                        'description' => $description));    
               
        return ['success'=>'Se ha actualizado el registro exitosamente', 'affected' => $qry];
    }


    /**
     * Obtiene la lista de Tecnologías.
     *
     * @param Request $request
     * @return array
     */
    public static function getTechList(Request $request)
    {
        // Consultamos listado
        // return $this->getIndex()::tech()->get()->pluck('tech');
        return (new ExportDrController())->getTechList($request);
    }

    /**
     * Obtiene la lista de Baselines.
     *
     * @param Request $request
     * @return array
     */
    public static function getBaselineList(Request $request)
    {
        return (new ExportDrController())->getBaselineList($request);
    }

    /**
     * Obtiene la lista de Templates.
     *
     * @param Request $request
     * @return array
     */
    public static function getTemplateList(Request $request)
    {
        return (new ExportDrController())->getTemplateList($request);
    }

    /**
     * Obtener raices para el arbol de la tecnología seleccionada. JsonResponse
     *
     * @param Request $request
     * @return array|JsonResponse
     */
    public function getTechTree(Request $request)
    {
        // Validamos input
       
        $validationRules = [
            'id' => 'required|string',
        ];
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
      
        return DbBaselineMOIndex::Tech()->select('technology')
                                        ->where('id_baseline', '=', $request->id)->get();
    }

       

    /**
     * Obtiene la lista de Tecnologías y MO para el arbol de datos.
     * Filtrados según los parámetros por Tecnología y Nombre parcial del MO.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterMOList(Request $request)
    {
        // Validamos input
        $validationRules = [
            'tech' => 'required|string',
            'mo' => 'nullable|string',
            'limit' => 'nullable|integer|min:1',
        ];
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        // Armamos el query
        $mo = $this->getIndex()::mo($request->input('mo'), $request->input('tech'));

        // Verificamos si se limitan los registros
        $limit = $request->input('limit');
        if (!empty($limit)) {
            $mo->limit($limit);
        }

        // Consultamos los datos
        return $mo->get();
    }

    /**
     * Obtiene la lista de Datos de la tabla asociada al MO seleccionado.
     * Con las exclusiones correspondientes.
     *
     * @param Request $request
     * @return array|JsonResponse
     */
    public function getDataParamsList(Request $request)
    {
        // Validamos input
        $validationRules = [
            'technology' => ['required',Rule::in($this->getIndexTechList())],
            'mo' => 'required|string',
            'id_baseline' => 'required|string'
        ];
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $dataList = $this->getIndex()::where($request->only(array_keys($validationRules)))->get();
         
        $table = $dataList[0]->mo_name;
        $id_baseline =  $dataList[0]->id_baseline;

        $qry = DB::connection('export_dr')
                ->table($table.' as t')
                ->whereIn('t.id',[$id_baseline,$request->fromTemplate])
                ->orderBy('t.id')
                ->get();
        
        $qry2 = DB::connection('export_dr')
                ->table('BaselineMoParam')
                ->select(DB::raw('param_name, concat(user_comment," - ",user_name," - ",last_update) as comment'))
                ->where('id_baseline',$id_baseline)                
                ->where('mo_name',$table)
                ->get();
        

         $rtemp = [];        
        foreach($qry2 as $q){
            $pName =  $q->param_name;
            $pComment = $q->comment;
            $rtemp = array_add($rtemp,$pName,$pComment);
        }


        //$pName =  $qry2[0]->param_name;
        //$pComment = $qry2[0]->comment;
        //$rtemp = array($pName => $pComment);

        $qry[] = $rtemp;

        
        //$result = $qry;
        // $result = array_add($qry2);
        /*$qry2 = DB::connection('export_dr')
                ->table($table.' as t')
                ->where('t.id',$request->fromTemplate)
                ->get();

        $result =array_merge((array)$qry,(array)$qry2);*/

        return $qry;

    }
    /**
     * Obtiene la lista de campos de la tabla asociada al MO seleccionado.
     * Con las exclusiones correspondientes.
     *
     * @param Request $request
     * @return array|JsonResponse
     */
    public function getParamsList(Request $request)
    {
        // Validamos input
        $validationRules = [
            'technology' => ['required',Rule::in($this->getIndexTechList())],
            'mo' => 'required|string',
        ];
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        // Campos a excluir
        //$excludeColumns = config('app.excludeColumns');

        // Obtengo registros seleccionados del indice
        /** @var Collection $indexList */
        $indexList = $this->getIndex()::where($request->only(array_keys($validationRules)))->get();

        if ($indexList->isNotEmpty()) {
            $table = $indexList[0]->mo_name;//TABLE_NAME_DB

            // Defino la conexión a usar
            $schema = Schema::connection('export_dr');

            // Obtengo listado de columnas de la tabla asociada al mo seleccionado
            $paramsList = $schema->getColumnListing($table);

            //foreach ($indexList as $index) {
                // Actualizamos campos a excluir
               // $excludeColumns[] = $index->COLUMN_NAME_DB;
            //}

            // Aplicamos filtrado de campos a excluir
            //$paramsList = array_values(array_diff($paramsList, $excludeColumns));MM

            // Ordenamos elementos alfabeticamente
            //sort($paramsList, SORT_NATURAL | SORT_FLAG_CASE);

            return $paramsList;

        } else {
            return response()->json(['error'=>'No se encontraron registros en el indice con los parámetros indicados'], 400);
        }
    }

    /**
     * Obtiene listado de parámetros filtrado por Tecnología seleccionada y
     * Nombre parcial del parámetro.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterParamsList(Request $request)
    {
        // Validamos input
        $validationRules = [
            'tech' => 'required|string',
            'param' => 'required|string',
            'limit' => 'nullable|integer|min:1',
        ];
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $tech = $request->input('tech');

        // Columnas a excluir
        $excludeColumns = config('app.excludeColumns');

        // Genero los placeholder para bind del arreglo
        $excludeColumnsBindings = implode(',', array_fill(0, count($excludeColumns), '?'));

        // Verificamos si se limitan los registros
        $limit = $request->input('limit');
        if (!empty($limit))
            $limit = "LIMIT $limit";

        if ($this->isHardware())
            $index = 'DB_HW_INDEX';
        else
            $index = 'DB_MO_Index';

        // Contruimos el query
        $sql = "SELECT DISTINCT ind.tech, ind.mo, col.column_name
                FROM $index ind
                  INNER JOIN information_schema.COLUMNS col ON ind.TABLE_NAME_DB = col.TABLE_NAME
                WHERE ind.TECH LIKE ?
                  AND col.TABLE_SCHEMA = ?
                  AND col.COLUMN_NAME LIKE ?
                  AND col.COLUMN_NAME NOT IN ($excludeColumnsBindings)
                  AND col.COLUMN_NAME NOT IN (SELECT i.COLUMN_NAME_DB
                                              FROM $index i
                                              WHERE i.TABLE_NAME_DB = col.TABLE_NAME)
                ORDER BY ind.MO, col.COLUMN_NAME
                $limit";

        // Armamos los parametros del query
        $params = array_merge([
            ($tech == '3G' ? $tech.'%' : $tech),                    // tecnologia seleccionada
            config('database.connections.export_dr.database'),  // nombre de la base de datos
            '%' . $request->input('param') . '%',               // nombre parcial del parametro
        ], $excludeColumns);                                        // columnas a excluir

        // Ejecutamos el query
        $rows = DB::connection('export_dr')->select($sql, $params);

        return response()->json($rows);
    }

    /**
     * Construye la instancia del query builder para su ejecución y reuso en distintos endpoints.
     *
     * @param Request $request
     * @param bool $withTemporal Indica si se debe consultar los cambios temporales
     * @param User $user
     * @return Builder|JsonResponse
     */
    public function buildQuery(Request $request, $withTemporal = true, $user = null)
    {
        /** @var User $user Usuario autenticado */
        $user = $user ? $user : auth()->user();

        // Build rules for filter params
        $columnsFilterRules = array_fill_keys($this->columnsFilter, 'nullable|string');

        // Validamos input
        $validationRules = [
                'planned_area_id' => ['nullable','integer','min:1',
                    Rule::exists((new PlannedArea())->getTable(), 'id')->where(function ($query) use ($user) {
                        return $query->where('user_id', $user->id);
                    })],
                //'date' => 'nullable|date',
                'technology' => ['required',Rule::in($this->getIndexTechList())],
                'mo' => 'required|string',
                'param' => 'required|string',
                'rowsPerPage' => 'nullable|integer|min:1',
            ] + $columnsFilterRules;

        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        // Obtenemos los elementos de red
        list($elemsRed, $columnsElemRed) = $this->getElemsRedByTechMO(
            $request->input('technology'),
            $request->input('mo'),
            $request->input('id_baseline')
        );

        // Buscamos los registros del indice
        $indexList = $this->indexList;

        if (count($indexList) > 0) {
            // Definimos los valores para el query
            $tech = $indexList[0]->technology;
            $mo = $indexList[0]->mo;
            $table = $indexList[0]->mo_name;
            //$param = $request->input('param'); MM

            //$setOrLdn = $this->getSetOrLdn($tech);

            // Armamos el query
            $qry = DB::connection('export_dr')
                ->table($table.' as t')
                //->selectRaw("DATE(varDateTime) AS date, t.`$setOrLdn`, $columnsElemRed `$param` AS current_value")
                //->whereRaw($this->buildWhere($request, $elemsRed, $param));
                 ->where('t.id_baseline',$request->input('id_baseline'));

            // Obtengo nombre de la base de datos
            $edr_db = $this->getEdrDb();

            if ($withTemporal) {
                // Verificamos si hay cambios en temporal del usuario
                /** @var MyChange $myTemporalChanges */
                $myTemporalChanges = $user->mychanges()->where('status', 'temporal')->first();

                if ($myTemporalChanges) {
                    $this->hasTemporalChanges = true;
                    $myChangesDetails = DB::table("$edr_db.my_changes_details")
                        ->where('my_changes_id', $myTemporalChanges->id);

                    $qry->leftJoinSub($myChangesDetails, 'mc', function ($join) use ($setOrLdn, $tech, $mo, $param, $elemsRed) {
                        $join->on(DB::raw('DATE(varDateTime)'), '=', 'mc.date')
                            ->on("t.$setOrLdn", '=', "mc.$setOrLdn")
                            ->where('mc.tech', '=', $tech)
                            ->where('mc.mo', '=', $mo)
                            ->where('mc.param', '=', $param);

                        foreach ($elemsRed as $name => $column) {
                            $join->on(DB::raw($column), '=', "mc.$name");
                        }
                    });

                    $qry->selectRaw('mc.new_value AS new_value');
                }
            }

            return $qry;

        } else {
            return response()->json(['error'=>'No se encontraron datos en el indice'], 400);
        }
    }

    /**
     * Construye las condiciones para el filtrado de la data.
     *
     * @param Request $request
     * @return string
     */
    protected function buildWhere(Request $request, $elemsRed, $valueColumn): string
    {
        // Definimos la fecha
        $date = $request->input('date') ?? $this->getDate()['date'];
        $where = "DATE(varDateTime) != '' ";
	    // DATE('$date')";

        // Se agregan los filtros de columnas si existen
        $filters = $request->only($this->columnsFilter);

        foreach ($filters as $field => $valor) {
            $field = str_after($field, 'filter_');

            if (array_key_exists($field, $elemsRed)) {
                $where .= " AND {$elemsRed[$field]} LIKE '%$valor%'";
            } else if (in_array($field, ['ldn','set'])) {
                $where .= " AND t.`$field` LIKE '%$valor%'";
            } else if ($field == 'new_value') {
                $where .= " AND t.`$valueColumn` LIKE '%$valor%'";
            }
        }

        $plannedAreaId = $request->planned_area_id;

        // Aplicamos filtro por area planificada
        if ($plannedAreaId && count($elemsRed) > 0) {
            // Obtengo nombre de la base de datos
            $edr_db = $this->getEdrDb();

            // Nombre de tabla
            $table = (new PlannedAreaCell())->getTable();

            $conditions = '';
            foreach ($elemsRed as $name => $column) {
                $conditions .= " AND `pa`.`$name` = t.$column";
            }

            $where .= " AND EXISTS (SELECT * 
                                    FROM `$edr_db`.`$table` AS `pa` 
                                    WHERE `pa`.`planned_area_id` = $plannedAreaId $conditions)";
        }

        return $where;
    }

    /**
     * Obtiene los datos de la tabla principal del dashboard.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getData(Request $request)
    {
        $qry = $this->buildQuery($request);

        if ($qry instanceof JsonResponse)
            return $qry;

        if ($request->showQry == '1')
            dd($qry->toSql());

        $tech = $request->tech;
        $mo = $request->mo;
        $param = $request->param;

        $result = $qry->paginate($request->input('rowsPerPage', 30))
            ->appends($request->except(['page']));

        $fixedData = null;
        if ($result->isNotEmpty()) {
            $fixedData = [
                'date' => $result->items()[0]->date,
                'tech' => $tech,
                'mo' => $mo,
                'param' => $param,
            ];
        }
        // Quitamos el campo date ya que está como campo fijo
        foreach ($result->items() as $item) {
            unset($item->date);
        }

        $result = ['fixedData'=>$fixedData, 'result'=>$result];

        return response()->json($result);
    }

    /**
     * Genera solicitud de descarga de los datos.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function exportData(Request $request)
    {
        $qry = $this->buildQuery($request);

        if ($qry instanceof JsonResponse)
            return $qry;

        if ($request->showQry == '1')
            dd($qry->toSql());

        $user = auth()->user();

        // Creamos registro de descarga
        $download = new Download([
            'user_id' => $user->id,
            'params' => \GuzzleHttp\json_encode($request->all()),
        ]);

        if ($download->save()) {

            $dataExport = new DataExport();
            $dataExport->requestParams = $request->all();
            $dataExport->userId = $user->id;
            $dataExport->downloadId = $download->id;
            if ($this->isHardware())
                $dataExport->controllerId = 'HW';
            else
                $dataExport->controllerId = 'DB';

            ExportDataToCsv::dispatch($dataExport);

            return response()->json([
                'status' => 'success',
                'title' => 'Hemos atendido su solicitud de descarga',
                'message' => 'En breve se le notificará el link de descarga'
            ]);

        } else {
            return response()->json(['error' => 'No se ha logrado atender su solicitud de descarga', 'message' => 'Intentalo más tarde'], 500);
        }
    }

    /**
     * Obtener la frecuencia de los datos.
     *
     * @param Request $request
     * @return array|JsonResponse
     */
    public function getDataFrequency(Request $request)
    {
        $qry = $this->buildQuery($request);

        if ($qry instanceof JsonResponse)
            return $qry;

        $param = $request->input('param');

        if ($this->hasTemporalChanges)
            $param = "COALESCE(mc.new_value, $param)";

        // Reemplazo el select y agrego la agrupación
        $qry->columns = [];
        $result = $qry->selectRaw("$param AS valor, count(*) AS frecuencia")
            ->groupBy('valor')
            ->get();

        if ($request->excel == true) {

            // se crea el csv en memoria
            $csv = Writer::createFromFileObject(new \SplTempFileObject());

            $csv->setOutputBOM(Writer::BOM_UTF8); // compatibilidad con windows
            
            // se inserta la primera fila con la cabecera
            $csv->insertOne(['valor', 'frecuencia']);

            // se transforma el contenido del resultado ya que la libreria solo acepta arrays
            $toCsv = $result->toArray();

            // varaible a ser enviada para la generacion del csv
            $newToCsv = array();
            
            // se itera sobre la data
            foreach ($toCsv as $element)
            {
                if (isset($element->valor) && isset($element->frecuencia)) {
                    $tempArray = array($element->valor, $element->frecuencia);
                    array_push($newToCsv, $tempArray);
                    $csv->insertOne($tempArray);
                }
            }

            //retorno utilizando esta funcion ya que envia los datos limpios
            return $csv->getContent();
        } else {
            return [
                'valores' => $result->pluck('valor'),
                'frecuencias' => $result->pluck('frecuencia'),
            ];
        }
    }

    /**
     * Obtiene los nombres de las columnas para la tabla de datos principal.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|array
     */
    public function getDataHeaders(Request $request)
    {
        // Validamos input
        $validationRules = [
            'tech' => 'required|string',
            'mo' => 'required|string',
        ];
        $validator = Validator::make($request->all(), $validationRules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $tech = $request->input('tech');
        $mo = $request->input('mo');
        $idBaseline = $request->input('id_baseline');//MM

        // Obtenemos los elementos de red
        $elemsRed = $this->getElemsRedByTechMO($tech, $mo, $idBaseline)[0];

        $columns = [];//'mo'

        if (!$this->isHardware())
            $columns[] = $tech == '2G' ? 'set' : 'ldn';

        $columns = array_merge($columns, array_keys($elemsRed));//, ['parametro', 'valor']);

        return $columns;
    }

    /**
     * Obtener fecha más reciente de actualización de los datos.
     *
     * @return array
     */
    public function getDate()
    {
        if (!isset($this->date)) {
            $this->date = LastUpdatedDate::query()->first()->DATE;
        }

        return ['date' => $this->date];
    }

    /**
     * Obtiene el arreglo de elementos de red y la cadena con las columnas para usar en el select del query.
     *
     * @param Collection|array $indexList
     * @return array
     */
    public function getElemsRed($indexList = null)
    {
        if (!isset($this->elemsRed)) {
            $indexList = $indexList ?? $this->indexList;

            // Genero las columnas de Elementos de Red
            $elemsRed = [];
            $columnsElemRed = '';
            foreach ($indexList as $index) {
                if (!empty($index->ELEMENTO_DE_RED) && !empty($index->COLUMN_NAME_DB)) {
                    $alias = strtolower($index->ELEMENTO_DE_RED);

                    if ($alias == 'celda_tdd' || $alias == 'celda_fdd') {
                        $alias = self::ELEM_RED_CELDA;
                        $elemsRed[$alias][] = "`{$index->COLUMN_NAME_DB}`";

                        if (count($elemsRed[$alias]) == 1)
                            $columnsElemRed .= "__CELDA__ AS '$alias',";
                    } else {
                        $elemsRed[$alias] = "`{$index->COLUMN_NAME_DB}`";
                        $columnsElemRed .= "`{$index->COLUMN_NAME_DB}` AS '$alias',";
                    }
                }
            }

            // Si existe 'celda' como elemento de red reemplazamos el marcador __CELDA__
            // por el COALESCE de cada columna
            if (isset($elemsRed[self::ELEM_RED_CELDA])
                && is_array($elemsRed[self::ELEM_RED_CELDA])
                && count($elemsRed[self::ELEM_RED_CELDA]) > 0) {

                $columns = implode(',', $elemsRed[self::ELEM_RED_CELDA]);
                $elemsRed[self::ELEM_RED_CELDA] = "COALESCE($columns)";
                $columnsElemRed = str_replace('__CELDA__', $elemsRed[self::ELEM_RED_CELDA], $columnsElemRed);
            }

            $this->elemsRed = [$elemsRed, $columnsElemRed];
        }

        return $this->elemsRed;
    }

    /**
     * Obtiene el arreglo de elementos de red y la cadena con las columnas para usar en el select del query
     * del MO y Tecnología seleccionados.
     *
     * @param $tech string Tecnología seleccionada
     * @param $mo string MO seleccionado
     * @return array
     */
    public function getElemsRedByTechMO($tech, $mo, $id)
    {
        // Buscamos los registros del indice
        if (!isset($this->indexList)) {
            $this->indexList = $this->getIndex()::where([
                'technology' => $tech, //MM
                'mo' => $mo,
                'id_baseline' => $id,
            ])->get();
        }

        return $this->indexList;//$this->getElemsRed();
    }

    public function getSetOrLdn($tech)
    {
        if (!isset($this->setOrLdn)) {
            $this->setOrLdn = $tech == '2G' ? 'set' : 'ldn';
        }
        return $this->setOrLdn;
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getEdrDb()
    {
        if (!isset($this->edr_db)) {
            $this->edr_db = config('database.connections.mysql.database');
        }
        return $this->edr_db;
    }
}
