<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyChangeDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'my_changes_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date','tech','mo','region','rnc','bsc','sitio','celda','set','ldn','param',
        'current_value','new_value','my_changes_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['my_changes_id','created_at','updated_at'];

    /**
     * MyChange entity which this instance belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mychange()
    {
        return $this->belongsTo(MyChange::class, 'my_changes_id');
    }
}
