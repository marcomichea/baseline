## Comandos artisan

Crear un modelo estandar con su migracion y su control
```
php artisan make:model <nombre> --migration --controller --resource
```

## Rutas de Resources

Agregar ruta de resource:
```
Route::resource('path', 'Controller');
```

Valores de resource
| Tipo    | Método    | Ruta           | Funcion        |
| :------ | :-------- | :------------- | :------------- |
| index   | GET       | path           | Lista          |
| create  | GET       | path/create    | Vista Nuevo    |
| store   | POST      | path           | Guarda Nuevo   |
| show    | GET       | path/{id}      | Vista Item     |
| edit    | GET       | path/{id}/edit | Vista Editar   |
| update  | PUT/PATCH | path/{id}      | Actualiza Item |
| destroy | DELETE    | path/{id}      | Borra Item     |
