-- Must be created in export_de_red original source DB not in edr_users
drop view LastUpdatedDate ;
create view LastUpdatedDate as 
  SELECT '2018-11-13' AS `DATE`;
