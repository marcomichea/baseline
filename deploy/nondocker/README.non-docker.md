Despliegue de Export de RED
===========================

Esta guía está orientada a la instalación del aplicativo y todas 
las dependencias necesarias para ejecutarlo sobre una distribución
Linux Ubuntu versión 16.04


## Despliegue de entorno de Desarrollo


Para desplegar el entorno de desarrollo:

0.  La instalación debe hacerse como el superusuario (root)

     su - root
        ó
     sudo -i


1.  Instalar repositorio de Export de RED

      - Clonar el repositorio de Export de RED

            >  cd $HOME
            >  git clone git@bitbucket.org:ai_ware/export_red_front.git

      - Seleccionar el tag correspondiente a la versión a implementar

            >  cd $HOME/export_de_red_front
            >  git checkout develop
            >  git fetch
            >  git pull
            >  git checkout tags/v0.9.0


2. Instalar algunos utilities

>       apt-get -y update
>       apt-get -y install libssl-dev wget curl nano gettext gcc g++ make git
>       apt-get -y install apt-transport-https python-software-properties build-essential 

 
3.  Instalar Apache2

>       apt-get -y update
>       apt-get -y install apache2

 - Para comprobar la instalación:
    * Verificar el estado del servicio

         > systemctl status apache2
         > service apache2 status

    * Acceder con el navegador desde una estación y se debe 
      apreciar la página inicial de Apache

	   
4.  Instalar PHP versión 7.1
     - Agregar repositorio externo:

          >     add-apt-repository ppa:ondrej/php

       Se solicitará confirmación para agregar el repositorio presionando ``<Enter>``.  Luego se procede a actualizar el repositorio

          >     apt-get -y update

      - Posteriormente se procede a instalar PHP y sus extensiones

          >     apt install php7.1 php7.1-xml php7.1-mbstring php7.1-mysql php7.1-json php7.1-curl php7.1-cli php7.1-common php7.1-mcrypt php7.1-gd libapache2-mod-php7.1 php7.1-zip

      - Para verificar la correcta instalación de PHP y sus extensiones

          >     php -v     # Debe mostrar la versión
          >     php -m   # Debe mostrar los módulos instalados

      - Para verificar la ejecución de PHP dentro de Apache, utilizamos la función "phpinfo"

          >     echo "<?php  phpinfo();  ?>" >/var/www/html/info.php

      - Consultamos desde el navegador la página

          >     http://ip-address/info.php

       El resultado debe indicar las configuraciones de PHP en Apache


5.  Instalar Laravel versión 5.7 y API de la aplicación
      - Para instalar Laravel primeramente se procede a instalar PHP Composer

         >     curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


      - Luego se copia desde el repositorio, el contenido del directorio __backend__ hacia Apache

         >     rm -rf /var/www/html/export_de_red
         >     mkdir -p /var/www/html/export_de_red
         >     cp -pr $HOME/export_de_red_front/backend/* /var/www/html/export_de_red/


      - Se ejecuta Composer para agregar las dependencia

         >     cd /var/www/html/export_de_red
         >     composer --verbose install


      - Se deben establecer los siguientes permisos y accesos:

         >  chown -R www-data:www-data /var/www/html/export_de_red
         >  chmod -R 755 /var/www/html/export_de_red/storage


      - Se procede a reconfigurar Apache para Laravel

      Se edita el archivo _/etc/apache2/sites-available/000-default.conf_
      Se modifica la directiva __DocumentRoot__ cambiando su valor original

         >     DocumentRoot /var/www/html


      por el valor

         >     DocumentRoot "/var/www/html/export_de_red/public"


      Luego se agrega la directiva de directorio que permiten funcionar adecuadamente a Laravel

```
     <Directory /var/www/html/export_de_red/public>
        Options +FollowSymlinks
        AllowOverride All
        Require all granted
     </Directory>
```


     La directiva debe agregarse justo antes de cerrar la declaración __</VirtualHost>__

     Se procede a habilitar los módulos de VirtualHost y reiniciar el servidor Apache
       
         >  a2enmod rewrite
         >  systemctl restart apache2
         >  service apache2 restart

         
      - Para probar la correcta implementación al abrir desde el navegador la URI http://ip-address/
        se debe apreciar el logo de Laravel
        También se puede obtener la versión de Laravel ejecutando:
        
        >   cd /var/www/html/export_de_red
        >   php artisan -V


      Ahora puede verificarse que el API ya está respondiendo peticiones.  Para probarlo, desde el navegador
      se accede a la URI _http://ip-address/api/user_ y la respuesta debe ser __"token_not_provided"__


6.  Instalar NodeJS versión 11
      - Descargar y ejecutar el instalador de los repositorios de NodeJS

            >  wget -O node_setup.sh https://deb.nodesource.com/setup_11.x
            >  sh ./node_setup.sh


      - Ejecutar la instalación de NodeJS

            >  apt-get -y install nodejs
            >  node -v
            >  npm -v


      - Ejecutar la instalación de complementos y utilitarios que puedan requerirse

            >  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
            >  echo "deb https://dl.yarnpkg.com/debian/ stable main" >/etc/apt/sources.list.d/yarn.list
            >  apt-get -y update
            >  apt-get -y install yarn
            >  yarn -v


      - Ejecutar la instalación de Angular CLI
            >  npm install -g @angular/cli@6
            >  ng -v


      - Instalar las dependencias para __Frontend__ de Export de RED

            >  cd $HOME/export_de_red_front/frontend
            >  npm --verbose --global install @angular/core rxjs zone.js @angular/common @angular/cli 
            >  # npm --verbose install patch-package --save-dev
            >  yarn add --dev patch-package postinstall-prepare
            >  npm --verbose install 


      - Configurar el ambiente para el Frontend.  Cambiar la URI en el archivo de ambiente

            >  vi $HOME/export_de_red_front/frontend/src/environments/environment.ts


        Adaptar la URI de la API cambiando la URI presente por algo similar a esto

            >  baseUrl: 'http://ip-address:port/api',



      - Iniciar el servidor

            >  cd $HOME/export_de_red_front/frontend
            >  npm start




7.  Instalar MySQL

        >  apt-get -y update
        >  apt-get -y install mysql-server mysql-client

      - Seguir los pasos interactivos de instalación y configuración que se indiquen


8.  Crear la Base de Datos
    Hay dos variantes de instalación de la base de datos según aplique el caso. Nos referiremos siempre
    como __servidorA__ a aquél donde está instalada y operativa la base de datos "export_de_red"

      - Caso #1
        La Base de Datos "export_de_red" está ubicada en un servidor, el cual para los efectos de
        este manual llamaremos __servidorA__.  La Base de Datos de la aplicación estará en un
        servidor distinto llamado __servidorB__ para efectos de entiendimiento de este manual

        Para que la conectividad entre bases de datos funcione correctamente se debe habilitar el modo
        federativo en la configuración de MySQL del __servidorA__

        - Habilitar modo federativo en __servidorA__

          Se debe agregar la cláusula __federated__ en la sección __[mysqld]__ preexistente de la
          configuración de MySQL
          Se accede al archivo de configuración de MySQL.  El mismo puede estar en /etc/my.cnf o bien
          en /etc/mysql/mysql.conf.d/mysqld.cnf generalmente.  Luego se busca la sección [mysqld] y se
          agrega la cláusula __federated__.  Por ejemplo:


             >  [mysqld]     #  <--- Sección ya existente
             >  federated    #  <--- Agregar cláusula


          Luego reiniciar el servidor de MySQL


             >  systemctl restart mysql
             >  service mysql restart


          Para comprobar que el modo _federativo_ está activo, acceder a la consola de MySQL y
          ejecutar:


             >  show engines \G


          Debe mostrar al final el modo ya activado:

             >  ********** 9. row ********** 
             >        Engine: FEDERATED
             >       Support: YES
             >  ....

        - Crear BD de la aplicación en __servidorA__ (federada) y en __servidorB__ (no federada)

          En __servidorA__ creamos una base de datos llamada, por ejemplo, "edr_users".  Al entrar en
          la consola de MySQL con un usuario con privilegios ("root") ejecutar


            >  CREATE DATABASE edr_users;
            >  CREATE USER edr_users identified by 'edr_users';
            >  GRANT ALL PRIVILEGES ON edr_users.* TO 'edr_users'@'%';
            >  FLUSH PRIVILEGES;


          En __servidorB__ creamos una base de datos con el mismo nombre "edr_users".  Al entrar en
          la consola de MySQL con un usuario con privilegios ("root") ejecutar


            >  CREATE DATABASE edr_users;
            >  CREATE USER edr_users identified by 'edr_users';
            >  GRANT ALL PRIVILEGES ON edr_users.* TO 'edr_users'@'%';
            >  FLUSH PRIVILEGES;


          Esta BD en __servidorB__ será aquella a la que se conecta la aplicación para almacenar los cambios


      - Caso #2.  La Base de Datos del aplicativo estará en el mismo servidor donde se
        encuentra la Base de Datos "export_de_red", al que llamaremos __servidorA__
        Se deben seguir los mismos pasos del Caso #1 excepto los referidos al __servidorB__


9. Crear la estructura de la base de datos

   Una vez existe la BD edr_users se procede a configurar en la API la conexión a la BD
   "export_de_red" y a la BD "edr_users"

     - Estando en el directorio de la API (Backend/Laravel) se crea el archivo de configuración
       (.env) a partir del modelo (.env.example) y se modifica con un editor de textos (nano, vim)


         >  cd /var/www/html/export_de_red
         >  ls -la .env*
         >  cp  .env.example  .env
         >  ls -la .env*
         >  vi .env         #  ó  nano .env


     - Se modifica en el archivo oculto .env la sección correspondiente a la base de datos de
       la aplicación


         >  DB_HOST=xxx.xxx.xxx.xxx           # <--- IP/nombre (caso #1 __servidorB__ / caso #2 __servidorA__)
         >  DB_PORT=3306                      # <--- Puerto utilizado para MySQL en servidor (según caso #1 ó #2)
         >  DB_DATABASE=edr_users             # <--- BD del aplicativo
         >  DB_USERNAME=edr_users             # <--- Usuario de la BD para "edr_users"
         >  DB_PASSWORD=xxxxxxxxx             # <--- Contraseña del usuario "edr_users"


     - Luego se modifica la sección correspondiente a la BD "export_de_red"


         >  EDR_DB_HOST=xxx.xxx.xxx.xxx       # <--- IP/nombre de __servidorA__
         >  EDR_DB_PORT=3306                  # <--- Puerto utilizado para MySQL en __servidorA__
         >  EDR_DB_DATABASE=export_de_red     # <--- BD "export_de_red" en __servidorA__
         >  EDR_DB_USERNAME=aiware            # <--- Usuario de la BD para "export_de_red"
         >  EDR_DB_PASSWORD=xxxxxx            # <--- Contraseña del usuario de la BD "export_de_red"


     - Con esta información agregada al archivo de configuración de la API (.env) se procede a
       crear la estructura de la base de datos en __servidorB__ a través de los comandos de
       migración de Laravel


         >  cd /var/www/html/export_de_red
         >  php artisan migrate


       Se crea la estructura de tablas necesarias para el aplicativo:

         >  use edr_users;
         >  show tables;
         >     +---------------------+
         >     | Tables_in_edr_users |
         >     +---------------------+
         >     | migrations          |
         >     | my_changes          |
         >     | my_changes_details  |
         >     | password_resets     |
         >     | planned_area_cells  |
         >     | planned_areas       |
         >     | users               |
         >     +---------------------+


10. Crear las tablas federadas si el caso es el #1

    Si la BD "edr_users" está en un servidor distinto a "export_de_red" se deben crear las tablas federadas
    en correspondencia al caso #1

    - Para ejecutar el script de creación de las tablas federadas primero debe crearse el ambiente en el
      shell con las variables necesarias

       >  cd $HOME/export_de_red_front/deploy
       >  export  DB_HOST=servidorB  DB_PORT=3306  DB_DATABASE=edr_users
       >  export  DB_USERNAME=edr_users  DB_PASSWORD=xxxxxxxxx
       >  envsubst < create_federated_tables.sql.tmpl >createfedtables.sql


    - Conectarse a través de la consola de MySQL a la base de datos "edr_users" creada en __servidorA__,
      la cual debería estar sin tablas creadas a este punto.  Luego ejecutar el script derivado del
      template (_createfedtables.sql_) en __servidorA__


       >  mysql --host=servidorA -uedr_users -pedr_users --database=edr_users
       >    source createfedtables.sql
       >    show tables;
       >       +---------------------+
       >       | Tables_in_edr_users |
       >       +---------------------+
       >       | my_changes_details  |
       >       | planned_area_cells  |
       >       +---------------------+


11. Configurar cuenta de correo para envío de contraseña

   Se procede por último a configurar las variables para gestionar el envío por correo de la contraseña
   recuperada.  El servicio habilitado para envío de correo funciona a través de SMTP y encriptamiento TLS

   - Estando en el directorio de la API (Backend/Laravel) se modifica el archivo de configuración
     (.env) con un editor de textos (nano, vim) la sección referida a correo


       >  MAIL_HOST=smtp.servidor.com             #  <-- Nombre o IP del servidor de correo
       >  MAIL_PORT=25                            #  <-- Puerto SMTP
       >  MAIL_USERNAME=usuario                   #  <-- Usuario quien envía el correo (sólo usuario o cuenta
       >                                          #      de correo según requerimiento del servidor de correo)
       >  MAIL_PASSWORD=xxxxxxxxxxxxxxxx          #  <-- Contraseña (no puede llevar espacios en blanco)
       >  MAIL_FROM_ADDRESS=usuario@servidor.com  #  <-- Se repite MAIL_USERNAME si es una cuenta de correo
       >                                          #      Si MAIL_USERNAME es un usuario sin cuenta de correo
       >                                          #      se debe escribir aquí la cuenta de correo origen
       >  MAIL_FROM_NAME=usuario@servidor.com     #  <-- Nombre descriptivo y correo o sólo correo


12. Acceso al aplicativo

   Una vez instalado el aplicativo crea un administrador inicial.  Las credenciales de acceso por defecto:


      >  Administrador por defecto: admin@admin.com
      >  Contraseña prestablecida:  12345678



-----

13. Despliegue del aplicativo

# Se debe utilizar sudo sólo cuando se requiera
cd $HOME
# cd /home/aiware

# Respaldar node_modules de frontend
cd $HOME/export_red_front/frontend
tar cvzf $HOME/node_modules_antes_20190321.tgz node_modules

# Respaldar Backend actual
cd /var/www/html
tar -cvzf $HOME/export_de_red_antes_20190321.tgz export_de_red_front

# Actualizar repositorio para despliegue
cd $HOME/export_red_front
git fetch
git checkout .
git tag
git checkout tags/v0.9.1
git status
#HEAD detached at v0.9.1
#nothing to commit, working directory clean

### Desplegar Backend
cd $HOME/export_red_front
cp -pr backend/* /var/www/html/export_de_red/
chown -R www-data:www-data /var/www/html/export_de_red/
# Reinciar Apache
# systemctl restart apache2  # No es indispensable

# Actualizar composer sólo si hace falta
cd /var/www/html/export_de_red/
mv ./composer.lock ./composer.lock.old  # Sólo para actualizar todo, si es realmente necesario
composer install
chown -R www-data:www-data /var/www/html/export_de_red/

# Ejecutar migraciones
php artisan migrate
# Migrating: 2019_01_18_184541_add_responsable_cluster_fields
# Migrated:  2019_01_18_184541_add_responsable_cluster_fields
# Migrating: 2019_02_08_165029_create_jobs_table
# Migrated:  2019_02_08_165029_create_jobs_table
# Migrating: 2019_02_08_172855_create_failed_jobs_table
# Migrated:  2019_02_08_172855_create_failed_jobs_table
# Migrating: 2019_03_04_162634_create_downloads_table
# Migrated:  2019_03_04_162634_create_downloads_table
### Fin despliegue Backend

### Desplegar Frontend
# Detener proceso de nodeJS actual
systemctl stop export_red.service
cd $HOME/export_red_front/frontend
npm --verbose install
npm --verbose start
# Compiles successfully
# Abortar con Ctrl-C

# Iniciar servicio export de red
systemctl start export_red.service
systemctl status export_red.service
