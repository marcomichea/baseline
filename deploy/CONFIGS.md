
Configuraciones
---------------

A continuación se explican algunas configuraciones que deben ajustarse al
realizar el despliegue de los contenedores

-----------------------------

Archivo __.env__

      
     TYPENV=dev   # <-- Determinar el ambiente de los contenedores
     DBG=0        # <-- Cuando es mayor a 3 permite ver la ejecución paso a paso de los scripts
     UID=1000     # <-- Debe ser igual al "id" del usuario actual
     GID=1000     # <-- Debe ser igual al "id" del grupo del usuario
     API_URL=http://api   # <-- Debe ser la URL a la que se accede al API
     API_PORT=8080        # <-- Puerto para acceder al API.  Se combinan ambas en un único URI
                          #     Si el puerto es __80__ no puede omitirse debe estar presente el valor
      


-----------------------------

Archivo __api.env__

El archivo tiene varios valores que pueden configurarse, sin embargo, los que deben
ajustarse necesariamente son los de conexión a las bases de datos


      
     # URI del aplicativo 
     APP_URL=http://app
     # 
     # Base de datos del aplicativo
     DB_HOST=db              # <----  No amerita cambio
     DB_PORT=3306            # <------|
     DB_DATABASE=edr_users   # <------|
     DB_USERNAME=edr_users   # <------+
     DB_PASSWORD=xxxxxxxxx
     #
     # Fuente de datos externa
     EDR_DB_HOST=servidor
     EDR_DB_PORT=3306
     EDR_DB_DATABASE=export_de_red
     EDR_DB_USERNAME=userschema
     EDR_DB_PASSWORD=xxxxxx
       



#EOMD
