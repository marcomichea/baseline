#!/bin/sh
#
# Initial run for Node/Angular docker container
# Project: export_de_red
# Author: dromero@innova4j.com
#

#set -e

DBG=${DBG:=0}

LOGDIR=/var/log
APPDIR=${APPDIR:=/app}
STPDIR=/.setup
CFGDIR=/etc/node/app
MODDIR=$APPDIR/node_modules
TMPDIR=/tmp

YARN_VERSION=${YARN_VERSION:=1.9.4}

ENV=${ENV:=dev}

NMPSTART=${NPMSTART:=start}
TPLTS=${TPLTS:=$STPDIR/environment.prod.ts.tmpl}
ENVTS=${ENVTS:=$APPDIR/src/environments/environment.prod.ts}

if [ $TYPENV = dev ]
then
  TPLTS=`dirname $TPLTS`/`basename $TPLTS .prod.ts.tmpl`.ts.tmpl
  ENVTS=`dirname $ENVTS`/`basename $ENVTS .prod.ts`.ts
fi

export LOGDIR APPDIR STPDIR CFGDIR MODDIR TMPDIR
export NPMSTART TPLTS ENVTS TYPENV YARN_VERSION ENV

echo "=== Starting Node Server @ `date` ..."

cd $HOME
mkdir -p $APPDIR $APPDIR/patches $STPDIR $CFGDIR $TMPDIR $HOME

[ $DBG -gt 1 ] && VRB="--verbose"
[ $DBG -gt 2 ] && env | sort && echo "--------------------"
[ $DBG -gt 3 ] && set -x

[ -d $APPDIR ] || echo "--- No existe directorio $APPDIR"
ls $APPDIR/* >/dev/null 2>&1 || echo "--- Directorio $APPDIR está vacío"

rm -f $ENVTS
envsubst <$TPLTS >$ENVTS

if [ $DBG -gt 0 ]
then
  pwd
  echo yarn version `yarn -v`
  tini --version
  npm version
  echo node version `node -v`
fi

cd $APPDIR

npm $VRB install

echo "=== Started Node Server @ `date` ...\n"

exec npm $VRB $NPMSTART $@



#EOS
