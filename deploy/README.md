Despliegue de contenedores para Export de RED
=============================================


## Despliegue de entorno de Desarrollo


Para desplegar el entorno de desarrollo:


1.  Clonar el repositorio __"https://bitbucket.org/ai_ware/export_red_front/src/DEV/"__ 

2.  Dentro del directorio/repositorio __"export_de_red_front"__
    acceder al directorio _"deploy"_

3.  Si no existen los archivos de configuracion deben crearse antes de instalar
    *  Copiar el archivo __default.env__ al archivo __.env__
    *  Copiar el archivo __api.default.env__ al archivo __api.env__
    *  Cambiar las configuraciones necesarias en __.env__ y en __api.env__
       
      
     # Acceder al directorio __deploy__
     cd export_de_red_fron/deploy
     #
     # Crear archivos de configuraciones
     cp  default.env  .env
     cp  api.default.env  api.env
     vi  .env  api.env


4.  Realizar el despliegue

      
     # Acceder al directorio __deploy__
     cd export_de_red_fron/deploy
     #
     # Construir los contenedores
     docker-compose build
     # Iniciar los contenedores
     docker-compose up -d
     #
     # Esperar unos dos minutos mientras se actualizan los contenedores
     sleep 120
     #
     # Revisar que todos los contenedores iniciaron
     docker-compose ps
     # Todos los contenedores indican "Up"
     #
     # Inicializar la base de datos (ejecutar migraciones)
     docker-compose exec api php artisan migrate
     #
     # Reiniciar los contenedores
     docker-compose down
     docker-compose up -d
       



### Requisitos

*  Instalar el siguiente paquete: __gettext__
*  Instalar __docker-ce__ version 18.x ó superior
*  Instalar __docker-compose__ version 1.22 ó superior


---


### Notas adicionales

El archivo _api.env_ en el directorio __deploy__ sobrescribe al
archivo _.env_ ubicado en el directorio __"export_de_red_front/backend"__

El archivo _environment.ts_ en el directorio __"export_de_red_front/backend"__
dentro de __frontend/src/environments/__ es sustituido por la versión ajustada
según las variables de ambiente en el despliegue del contenedor _"app"_

Al ejecutar las migraciones se crea también el primer administrador del sistema
Por defecto el login del administrador es __admin@admin.com__ y la contraseña inicial
es __xxxxxxxx__

Mayor detalle de los archivos de configuraciones en el
archivo CONFIGS.md en el mismo directorio __deploy__


-----
